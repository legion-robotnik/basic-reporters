import arrow
from collections import namedtuple
from legion_utils import Priority
from pytest import mark
from pytest import mark
from pytest_mock import MockerFixture
from socket import gethostname

from reporters.borg_backup import env, cmd, snapshots, Snapshot, seconds_since_last, humanize_latest
from reporters.borg_backup import prio, delayed_snapshot, borg_list, backups, report, run

ONE_DAY_AGO = arrow.now().shift(days=-1)
ONE_WEEK_AGO = arrow.now().shift(weeks=-1)
ONE_MONTH_AGO = arrow.now().shift(months=-1)
TWO_MONTH_AGO = arrow.now().shift(months=-2)

BORG_LIST = bytes(f'''
mr-bones-2021-03-01_16-07-55Z        {TWO_MONTH_AGO.format('ddd, YYYY-MM-DD HH:mm:ss')} [943606f990345d359c166688a6b02dc91e0a032a648b7fe24125744689b6c8ba]
mr-bones-2021-06-01_00-13-34Z        {ONE_MONTH_AGO.format('ddd, YYYY-MM-DD HH:mm:ss')} [ade98c893bc9c526aff9d7ace348e7fbde386859e150c85b5dddd0091aa71f40]
''', 'utf-8')
BORG_LIST_SNAPSHOTS = [Snapshot('mr-bones-2021-03-01_16-07-55Z',
                                TWO_MONTH_AGO.int_timestamp,
                                '943606f990345d359c166688a6b02dc91e0a032a648b7fe24125744689b6c8ba'),
                       Snapshot('mr-bones-2021-06-01_00-13-34Z',
                                ONE_MONTH_AGO.int_timestamp,
                                'ade98c893bc9c526aff9d7ace348e7fbde386859e150c85b5dddd0091aa71f40'),]
BORG_LIST_RECENT = bytes(f'''
mr-bones-2021-03-01_16-07-55Z        {ONE_WEEK_AGO.format('ddd, YYYY-MM-DD HH:mm:ss')} [943606f990345d359c166688a6b02dc91e0a032a648b7fe24125744689b6c8ba]
mr-bones-2021-06-01_00-13-34Z        {ONE_DAY_AGO.format('ddd, YYYY-MM-DD HH:mm:ss')} [ade98c893bc9c526aff9d7ace348e7fbde386859e150c85b5dddd0091aa71f40]
''', 'utf-8')
BORG_LIST_RECENT_SNAPSHOTS = [Snapshot('mr-bones-2021-03-01_16-07-55Z',
                                       ONE_WEEK_AGO.int_timestamp,
                                       '943606f990345d359c166688a6b02dc91e0a032a648b7fe24125744689b6c8ba'),
                              Snapshot('mr-bones-2021-06-01_00-13-34Z',
                                       ONE_DAY_AGO.int_timestamp,
                                       'ade98c893bc9c526aff9d7ace348e7fbde386859e150c85b5dddd0091aa71f40'),]
BORG_LIST_FAIL = bytes('Failed to create/acquire the lock /backups/borg/mr-bones/lock.exclusive (timeout).', 'utf-8')

TEST_CONFIG = {
    "exchange": "skynet",
    "route": "mr-bones.system.borg_backups",
    "ttl": 1200,
    "repositories": {
        "ed-209:/local/borg/mr-bones": {
            "passphrase": "hackme",
            "local_user": "eugene",
            "warn_threshold_seconds": 172800,
            "err_threshold_seconds": 432000,
            "crit_threshold_seconds": 604800
        },
        "ed-210:/local/borg/mr-bones": {
            "passphrase": "hackme",
            "local_user": "eugene",
            "warn_threshold_seconds": 172800,
            "err_threshold_seconds": 432000,
            "crit_threshold_seconds": 604800
        }
    }
}

ProcResult = namedtuple('ProcResult', ['stdout'])


@mark.parametrize('user,exp_cmd', [
    (None, ['bash', '-c', 'borg list']),
    ('eugene', ['su', 'eugene', '-c', 'borg list']),
    ('', ['su', '', '-c', 'borg list'])
])
def test_cmd(user, exp_cmd):
    assert cmd(user) == exp_cmd


@mark.parametrize('repo,passph,exp_env', [
    ('ed-209:/local/borg/mr-bones', 'hackme', {'BORG_REPO': 'ed-209:/local/borg/mr-bones', 'BORG_PASSPHRASE': 'hackme'}),
])
def test_env(repo, passph, exp_env):
    assert all(i in env(repo, passph).items() for i in exp_env.items())


@mark.parametrize('stdout, exp', [
    (BORG_LIST, BORG_LIST_SNAPSHOTS),
    (BORG_LIST_FAIL, []),
    (None, []),
])
def test_snapshots(stdout, exp):
    assert snapshots(stdout) == exp


@mark.parametrize('snaps,exp', [
    (BORG_LIST_SNAPSHOTS, arrow.now().int_timestamp - ONE_MONTH_AGO.int_timestamp)
])
def test_seconds_since_last(snaps, exp):
    assert abs(seconds_since_last(snaps) - exp) <= 1  # result should be within one second of expectation


@mark.parametrize('snaps,exp', [
    (BORG_LIST_SNAPSHOTS, arrow.now().shift(seconds=-(seconds_since_last(BORG_LIST_SNAPSHOTS))).humanize())
])
def test_humanize_latest(snaps, exp):
    assert humanize_latest(snaps) == exp


@mark.parametrize('seconds,config,exp', [
    (seconds_since_last(BORG_LIST_SNAPSHOTS), TEST_CONFIG['repositories']["ed-210:/local/borg/mr-bones"], Priority.CRITICAL)
])
def test_prio(seconds, config, exp):
    assert prio(seconds, config) == exp


@mark.parametrize('snaps,config,repo,expected', [
    (BORG_LIST_SNAPSHOTS, TEST_CONFIG, 'ed-210:/local/borg/mr-bones', (TEST_CONFIG['exchange'], TEST_CONFIG['route'],
                                                                       f'Last successful backup from {gethostname()} to ed-210:/local/borg/mr-bones was {humanize_latest(BORG_LIST_SNAPSHOTS)}',
                                                                       f'[{gethostname()}][borg-backup][ed-210:/local/borg/mr-bones][too_long]',
                                                                       {'problem_repo': s._asdict() for s in BORG_LIST_SNAPSHOTS},
                                                                       TEST_CONFIG['ttl'], Priority.CRITICAL)),
    (BORG_LIST_RECENT_SNAPSHOTS, TEST_CONFIG, 'ed-210:/local/borg/mr-bones', None)
])
def test_delayed_snapshot(snaps, config, repo, expected):
    assert delayed_snapshot(snaps, config, repo) == expected


@mark.parametrize('borg_list_output,expected',[
    (BORG_LIST, BORG_LIST_SNAPSHOTS),
])
def test_borg_list(mocker: MockerFixture, borg_list_output, expected):
    def mock_run(cmd, env, check, stdout):
        assert cmd == ['su', '__user__', '-c', 'borg list']
        assert env['BORG_REPO'] == '__repo__'
        assert env['BORG_PASSPHRASE'] == '__passphrase__'
        assert not check
        return ProcResult(borg_list_output)
    mocker.patch('subprocess.run', mock_run)
    assert borg_list('__repo__', '__passphrase__', '__user__') == expected


@mark.parametrize('borg_list_output,config,expected',[
    (BORG_LIST, {'repositories': {'__repo__': {'passphrase': '__passphrase__', 'local_user': '__user__'}}}, BORG_LIST_SNAPSHOTS),
])
def test_backups(mocker: MockerFixture, borg_list_output, config, expected):
    def mock_run(cmd, env, check, stdout):
        assert cmd == ['su', '__user__', '-c', 'borg list']
        assert env['BORG_REPO'] in config['repositories']
        assert env['BORG_PASSPHRASE'] == config['repositories'][env['BORG_REPO']]['passphrase']
        assert not check
        return ProcResult(borg_list_output)
    mocker.patch('subprocess.run', mock_run)
    assert backups(config['repositories']) == {repo: expected for repo in config['repositories']}


@mark.parametrize('borg_list_output,config,expected',[
    (BORG_LIST, {'repositories': {'__repo__': {'passphrase': '__passphrase__', 'local_user': '__user__'}}}, BORG_LIST_SNAPSHOTS),
    (BORG_LIST, {'repositories': {'__repo2__': {'passphrase': '__passphrase2__', 'local_user': '__user2__'}}}, BORG_LIST_SNAPSHOTS),
    (BORG_LIST_RECENT, {'repositories': {'__repo__': {'passphrase': '__passphrase__', 'local_user': '__user__'}}}, BORG_LIST_RECENT_SNAPSHOTS),
    (BORG_LIST_RECENT, {'repositories': {'__repo2__': {'passphrase': '__passphrase2__', 'local_user': '__user2__'}}}, BORG_LIST_RECENT_SNAPSHOTS),
    (BORG_LIST_FAIL, {'repositories': {'__repo__': {'passphrase': '__passphrase__', 'local_user': '__user__'}}}, []),
    (BORG_LIST_FAIL, {'repositories': {'__repo2__': {'passphrase': '__passphrase2__', 'local_user': '__user2__'}}}, []),
])
def test_report(mocker: MockerFixture, borg_list_output, config, expected):
    def mock_run(cmd, env, check, stdout):
        assert cmd == ['su', config['repositories'][env['BORG_REPO']]['local_user'], '-c', "borg list"]
        assert env['BORG_REPO'] in config['repositories']
        assert env['BORG_PASSPHRASE'] == config['repositories'][env['BORG_REPO']]['passphrase']
        assert not check
        return ProcResult(borg_list_output)
    mocker.patch('subprocess.run', mock_run)
    assert report(config['repositories']) == {repo: [s._asdict() if s else None for s in expected] for repo in config['repositories']}


@mark.parametrize('config,borg_list_output,expected_alert_desc,expected_alert_priority,expected_info_contents',[
    ({'exchange': '__exchange__', 'route': '__route__', 'ttl': 666,
                 'repositories': {'__repo__': {'passphrase': '__passphrase__', 'local_user': '__user__', "warn_threshold_seconds": 172800,
                                               "err_threshold_seconds": 432000, "crit_threshold_seconds": 604800}}}, BORG_LIST,
     f'Last successful backup from {gethostname()} to __repo__ was {humanize_latest(BORG_LIST_SNAPSHOTS)}', Priority.CRITICAL,
     {'src': gethostname(), 'repositories': {repo: [s._asdict() for s in BORG_LIST_SNAPSHOTS] for repo in ['__repo__']}}),
    ({'exchange': '__exchange__', 'route': '__route__', 'ttl': 666,
                 'repositories': {'__repo__': {'passphrase': '__passphrase__', 'local_user': '__user__', "warn_threshold_seconds": 172800,
                                               "err_threshold_seconds": 432000, "crit_threshold_seconds": 604800}}}, BORG_LIST_RECENT,
     None, Priority.INFO, {'src': gethostname(), 'repositories': {repo: [s._asdict() for s in BORG_LIST_RECENT_SNAPSHOTS] for repo in ['__repo__']}}),
    ({'exchange': '__exchange__', 'route': '__route__', 'ttl': 666,
                 'repositories': {'__repo__': {'passphrase': '__passphrase__', 'local_user': '__user__', "warn_threshold_seconds": 172800,
                                               "err_threshold_seconds": 432000, "crit_threshold_seconds": 604800}}}, BORG_LIST_FAIL,
     None, Priority.INFO, {'src': gethostname(), 'repositories': {'__repo__': []}}),
])
def test_run(mocker: MockerFixture, config, borg_list_output, expected_alert_desc, expected_alert_priority, expected_info_contents):
    def mock_broadcast_info(exchange, route, contents):
        assert exchange == config['exchange']
        assert route == config['route']
        assert contents == expected_info_contents
        return None
    def mock_broadcast_alert(exchange, route, description, alert_key, contents, ttl, priority):
        assert exchange == config['exchange']
        assert route == config['route']
        assert priority == expected_alert_priority
        assert ttl == config['ttl']
        assert alert_key.startswith(f'[{gethostname()}][borg-backup][') and alert_key.endswith('][too_long]')
        assert description == expected_alert_desc
        assert 'problem_repo' in contents
        return None
    def mock_run(cmd, env, check, stdout):
        assert cmd == ['su', '__user__', '-c', 'borg list']
        assert env['BORG_REPO'] == '__repo__'
        assert env['BORG_PASSPHRASE'] == '__passphrase__'
        assert not check
        return ProcResult(borg_list_output)
    mocker.patch('subprocess.run', mock_run)
    mocker.patch('legion_utils.broadcast_info', mock_broadcast_info)
    mocker.patch('legion_utils.broadcast_alert', mock_broadcast_alert)
    assert run(config) == None