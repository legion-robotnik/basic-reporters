from socket import gethostname
from pathlib import Path

import legion_utils
from legion_utils import Priority
from pytest import mark
from pytest_mock import MockerFixture

from reporters.file_existence import run, notification_for

CONFIG = {"exchange": "skynet",
          "route": "mr-bones.systen.files",
          "ttl": 60,
          "paths": {
              "/mnt/remote-drive": {"state": "present",
                                    "priority": "activity"},
              "/usr/bin": {"state": "absent",
                           "priority": "critical"},
              "/usr/bin/python3": {"state": "absent",
                                   "priority": "error"},
              "/this/path/should/not/exist": {"state": "present",
                                              "priority": "error"}}}


@mark.parametrize('path, notify_if_present, priority, exp_exist', [
    ('/usr/bin', True, Priority.INFO, True),
    ('/usr/bin', True, Priority.ACTIVITY, True),
    ('/usr/bin', True, Priority.WARNING, True),
    ('/usr/bin', True, Priority.ERROR, True),
    ('/usr/bin', True, Priority.CRITICAL, True),
    ('/usr/bin', False, Priority.INFO, True),
    ('/usr/bin', False, Priority.ACTIVITY, True),
    ('/usr/bin', False, Priority.WARNING, True),
    ('/usr/bin', False, Priority.ERROR, True),
    ('/usr/bin', False, Priority.CRITICAL, True),
    ('/does/not/exist', True, Priority.INFO, False),
    ('/does/not/exist', True, Priority.ACTIVITY, False),
    ('/does/not/exist', True, Priority.WARNING, False),
    ('/does/not/exist', True, Priority.ERROR, False),
    ('/does/not/exist', True, Priority.CRITICAL, False),
    ('/does/not/exist', False, Priority.INFO, False),
    ('/does/not/exist', False, Priority.ACTIVITY, False),
    ('/does/not/exist', False, Priority.WARNING, False),
    ('/does/not/exist', False, Priority.ERROR, False),
    ('/does/not/exist', False, Priority.CRITICAL, False),
    ('/etc/*', True, Priority.INFO, True),
    ('/etc/*', True, Priority.ACTIVITY, True),
    ('/etc/*', True, Priority.WARNING, True),
    ('/etc/*', True, Priority.ERROR, True),
    ('/etc/no_exist/*', False, Priority.INFO, False),
    ('/etc/no_exist/*', False, Priority.ACTIVITY, False),
    ('/etc/no_exist/*', False, Priority.WARNING, False),
    ('/etc/no_exist/*', False, Priority.ERROR, False),
])
def test_notification(path, notify_if_present, priority, exp_exist):
    notification = notification_for(path, notify_if_present, priority, 60)
    if (notify_if_present and exp_exist) or (not notify_if_present and not exp_exist):
        assert notification is not None
        assert notification.priority == priority
        assert notification.ttl == 60
        if priority >= 2:
            assert notification.alert_key == f'[{gethostname()}][{path}][{"present" if exp_exist else "absent"}]'
            assert notification.desc == f'{gethostname()}:{path} {"present" if exp_exist else "absent"}'
    else:
        assert notification is None


def test_run_no_publish(mocker: MockerFixture):
    mocker.patch('legion_utils.broadcast_msg', return_value=None)
    config = {"exchange": "skynet",
              "route": "mr-bones.systen.files",
              "ttl": 60,
              "paths": {"/usr/bin": {"state": "absent",
                                     "priority": "critical"}}}
    run(config)
    legion_utils.broadcast_msg.assert_not_called()


def test_run_publish(mocker: MockerFixture):
    def mock_broadcast_msg(exchange, route, msg):
        assert exchange == 'skynet'
        assert route == 'mr-bones.systen.files'
        assert msg.priority == Priority.INFO
    mocker.patch('legion_utils.broadcast_msg', mock_broadcast_msg)
    config = {"exchange": "skynet",
              "route": "mr-bones.systen.files",
              "ttl": 60,
              "paths": {"/usr/bin": {"state": "present",
                                     "priority": "info"}}}
    run(config)
