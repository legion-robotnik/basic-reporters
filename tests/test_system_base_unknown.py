from socket import gethostname
from time import sleep

import arrow
from legion_utils import Priority
from pytest import mark
from pytest_mock import MockerFixture
from robotnikmq import Message

from reporters.system_base_unknown import run, NetworkState, now, seconds_since_observed
from reporters.system_base_unknown import seconds_since_reported

CONFIG1 = {"report_exchange": "skynet",
           "report_route_suffix": ".system.system_reporting",
           "ttl": 1,
           "hosts": {
               "example.com": {
                   "exchange": "skynet",
                   "binding": "example.com.system.system_base_stats.info",
                   "warning_after_seconds": 60,
                   "error_after_seconds": 300,
                   "critical_after_seconds": 600
               },
               "another_host.example.com": {
                   "exchange": "skynet",
                   "binding": "another_host.example.com.system.system_base_stats.info",
                   "warning_after_seconds": 60,
                   "error_after_seconds": 300,
                   "critical_after_seconds": 600
               }}}


def test_network_state_observe():
    state = NetworkState(CONFIG1['hosts'])
    state.observe('example.com')
    assert abs(now() - state['example.com'].last_observed) <= 1


def test_network_state_observe_nonexistent():
    state = NetworkState(CONFIG1['hosts'])
    state.observe('example1.com')
    assert 'example1.com' not in state.hosts


def test_seconds_since_last_observed():
    state = NetworkState(CONFIG1['hosts'])
    state.observe('example.com')
    assert seconds_since_observed(state['example.com']) <= 1


def test_network_state_report():
    state = NetworkState(CONFIG1['hosts'])
    state.report('example.com')
    assert abs(now() - state['example.com'].last_reported) <= 1


def test_seconds_since_last_reported():
    state = NetworkState(CONFIG1['hosts'])
    state.report('example.com')
    assert seconds_since_reported(state['example.com']) <= 1


def test_network_state_report_nonexistent():
    state = NetworkState(CONFIG1['hosts'])
    state.report('example1.com')
    assert 'example1.com' not in state.hosts


def test_alert_for_nonexistent():
    state = NetworkState(CONFIG1['hosts'])
    state.observe("example.com")
    assert state.alert_for("example1.com", 1) is None


def test_alert_for_never_observed():
    state = NetworkState(CONFIG1['hosts'])
    assert state.alert_for("example.com", 1).description == 'example.com has never been observed'


@mark.parametrize('config,delay,exp_priority', [
    (CONFIG1, 1, None),
    (CONFIG1, 60, Priority.WARNING),
    (CONFIG1, 300, Priority.ERROR),
    (CONFIG1, 600, Priority.CRITICAL),
])
def test_alert_for(config, delay, exp_priority):
    state = NetworkState(config['hosts'])
    state.observed("example.com", now() - delay)
    alert = state.alert_for("example.com", 1)
    if exp_priority is None:
        assert alert is None
    else:
        humanized = arrow.now().shift(seconds=(0 - delay)).humanize()
        assert alert.contents == {'src': gethostname(), 'last_observed': now() - delay}
        assert alert.key == '[example.com][system_reporting_failure]'
        assert alert.description == f'example.com last reported its status {humanized}'
        assert alert.priority == exp_priority


def test_alert_on(mocker: MockerFixture):
    def mock_broadcast_alert_msg(exchange, route, alert):
        humanized = arrow.now().shift(seconds=(0 - 600)).humanize()
        assert exchange == CONFIG1['report_exchange']
        assert route == 'example.com' + CONFIG1['report_route_suffix']
        assert alert.contents == {'src': gethostname(), 'last_observed': now() - 600}
        assert alert.key == '[example.com][system_reporting_failure]'
        assert alert.description == f'example.com last reported its status {humanized}'
        assert alert.priority == Priority.CRITICAL
        assert alert.ttl == CONFIG1['ttl']
    mocker.patch('legion_utils.broadcast_alert_msg', mock_broadcast_alert_msg)
    state = NetworkState(CONFIG1['hosts'])
    state.observed("example.com", now() - 600)
    state.alert_on('example.com', CONFIG1['report_exchange'], 'example.com' + CONFIG1['report_route_suffix'], CONFIG1['ttl'])
    assert seconds_since_reported(state['example.com']) == 0


def test_alert_on_nonexistent():
    state = NetworkState(CONFIG1['hosts'])
    assert state.alert_on('example1.com', CONFIG1['report_exchange'], 'example.com' + CONFIG1['report_route_suffix'], CONFIG1['ttl']) is None


def test_run(mocker: MockerFixture):
    def mock_bind(_, exchange, binding):
        assert exchange in [CONFIG1['hosts'][h]['exchange'] for h in CONFIG1['hosts']]
        assert binding in [CONFIG1['hosts'][h]['binding'] for h in CONFIG1['hosts']]

    def mock_consume(_, inactivity_timeout):
        assert inactivity_timeout == 3
        for i in [Message({'hostname': 'another_host.example.com'}), Message({'stuff': 'nothing'})]:
            yield i

    def mock_broadcast_alert_msg(exchange, route, alert):
        assert exchange == CONFIG1['report_exchange']
        assert route == 'example.com' + CONFIG1['report_route_suffix']
        assert alert.contents == {'src': gethostname(), 'last_observed': 0}
        assert alert.key == '[example.com][system_reporting_failure]'
        assert alert.description == 'example.com has never been observed'
        assert alert.priority == Priority.CRITICAL
        assert alert.ttl == CONFIG1['ttl']

    def mock_init(_):
        pass
    mocker.patch('legion_utils.broadcast_alert_msg', mock_broadcast_alert_msg)
    mocker.patch('reporters.system_base_unknown.Subscriber.__init__', mock_init)
    mocker.patch('reporters.system_base_unknown.Subscriber.bind', mock_bind)
    mocker.patch('reporters.system_base_unknown.Subscriber.consume', mock_consume)
    sleep(1.1)
    run(CONFIG1)
