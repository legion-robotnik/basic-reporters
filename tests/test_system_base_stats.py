from collections import namedtuple
from pathlib import Path
from socket import gethostname

import legion_utils
from pytest import mark
from pytest_mock import MockerFixture

from reporters.system_base_stats import run

Usage = namedtuple('Usage', ['percent'])


class DiskUsage():
    def __init__(self, stuff):
        self.stuff = stuff

    def _asdict(self):
        return self.stuff


@mark.parametrize('hostname,expected_output_hostname', [
    ('ED-209', 'ed-209'),
    ('mr-bones', 'mr-bones'),
    ('MR-bones', 'mr-bones')
])
def test_hostname_capitalization(mocker: MockerFixture, hostname, expected_output_hostname):
    config = {"queue": "skynet",
              "route": f"{hostname}.system.system_base_stats"}

    def mock_broadcast_info(exchange, route, contents):
        assert contents['hostname'] == expected_output_hostname
    
    mocker.patch('socket.gethostname', return_value=hostname)
    mocker.patch('legion_utils.broadcast_info', mock_broadcast_info)
    mocker.patch("psutil.cpu_percent", return_value=6.4)
    mocker.patch("psutil.virtual_memory", return_value=Usage(80.1))
    mocker.patch("psutil.disk_usage", return_value=DiskUsage({'free': 100001,
                                                              'percent': 60,
                                                              'total': 200001,
                                                              'used': 100000}))
    run(config)


@mark.parametrize("paths,disk_usage,exp_info_count,exp_warn_count,exp_err_count,exp_crit_count",
                  [([], 0, 1, 0, 0, 0),
                   (['/'], 0, 1, 0, 0, 0),
                   (['/', '/local'], 0, 1, 0, 0, 1),
                   ({"/": {'warning_threshold_used_percent': 90,
                           'error_threshold_used_percent': 95,
                           'critical_threshold_used_percent': 98}}, 0, 1, 0, 0, 0),
                   ({"/": {'warning_threshold_used_percent': 80,
                           'error_threshold_used_percent': 85,
                           'critical_threshold_used_percent': 88}}, 70, 1, 0, 0, 0),
                   ({"/": {'warning_threshold_used_percent': 90,
                           'error_threshold_used_percent': 95,
                           'critical_threshold_used_percent': 98}}, 90, 1, 1, 0, 0),
                   ({"/": {'warning_threshold_used_percent': 90,
                           'error_threshold_used_percent': 95,
                           'critical_threshold_used_percent': 98}}, 96, 1, 0, 1, 0),
                   ({"/": {'warning_threshold_used_percent': 90,
                           'error_threshold_used_percent': 95,
                           'critical_threshold_used_percent': 98}}, 99, 1, 0, 0, 1),
                   ({"/": {'warning_threshold_used_percent': 90,
                           'error_threshold_used_percent': 95,
                           'critical_threshold_used_percent': 98},
                     "/home": {'warning_threshold_used_percent': 80,
                               'error_threshold_used_percent': 85,
                               'critical_threshold_used_percent': 88}}, 70, 1, 0, 0, 0),
                   ({"/": {'warning_threshold_used_percent': 90,
                           'error_threshold_used_percent': 95,
                           'critical_threshold_used_percent': 98},
                     "/home": {'warning_threshold_used_percent': 80,
                               'error_threshold_used_percent': 85,
                               'critical_threshold_used_percent': 88}}, 85, 1, 0, 1, 0),
                   ({"/": {'warning_threshold_used_percent': 90,
                           'error_threshold_used_percent': 95,
                           'critical_threshold_used_percent': 98},
                     "/home": {'warning_threshold_used_percent': 80,
                               'error_threshold_used_percent': 85,
                               'critical_threshold_used_percent': 88}}, 99, 1, 0, 0, 2),
                   ({"/": {'warning_threshold_used_percent': 90,
                           'error_threshold_used_percent': 95,
                           'critical_threshold_used_percent': 98},
                     "/home": {'warning_threshold_used_percent': 80,
                               'error_threshold_used_percent': 85,
                               'critical_threshold_used_percent': 88}}, 90, 1, 1, 0, 1)
                   ])
def test_system_base_stats(mocker, paths, disk_usage,
                           exp_info_count, exp_warn_count, exp_err_count, exp_crit_count):
    config = {"queue": "skynet",
              "route": "mr-bones.system.system_base_stats"}
    if paths:
        config['paths'] = paths
    mocker.patch('legion_utils.broadcast_info', return_value=None)
    mocker.patch('legion_utils.broadcast_warning', return_value=None)
    mocker.patch('legion_utils.broadcast_error', return_value=None)
    mocker.patch('legion_utils.broadcast_critical', return_value=None)
    mocker.patch("psutil.cpu_percent", return_value=6.4)
    mocker.patch("psutil.virtual_memory", return_value=Usage(80.1))
    mocker.patch("psutil.disk_usage", return_value=DiskUsage({'free': 100001,
                                                              'percent': disk_usage,
                                                              'total': 200001,
                                                              'used': 100000}))
    run(config)
    assert legion_utils.broadcast_info.call_count == exp_info_count
    assert legion_utils.broadcast_warning.call_count == exp_warn_count
    assert legion_utils.broadcast_error.call_count == exp_err_count
    assert legion_utils.broadcast_critical.call_count == exp_crit_count
    if exp_info_count > 0:
        usage = {'free': 100001, 'percent': disk_usage, 'total': 200001, 'used': 100000}
        contents = {'CPU_load': 6.4, 'RAM_usage': 80.1, 'core_load': 6.4, 'hostname': gethostname()}
        if paths:
            contents['disks'] = {p: usage for p in paths if Path(p).exists()}
        legion_utils.broadcast_info.assert_called_once_with(exchange=config['queue'],
                                                            route=config['route'],
                                                            contents=contents)
