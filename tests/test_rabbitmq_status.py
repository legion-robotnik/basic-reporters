from pytest_mock import MockerFixture

from legion_utils import Priority

from reporters.rabbitmq_status import RabbitMQClusterStatus, CLUSTER_STATUS_PAT, cluster_status
from reporters.rabbitmq_status import disk_node_expectations, running_node_expectations, run
from reporters.rabbitmq_status import version_mismatch_priority, alarm_priority, network_partition_priority

RABBITMQCTL_CLUSTER_STATUS = bytes('''
00:25:02.415 [warn]  Description: 'Authenticity is not established by certificate path validation'
     Reason: 'Option {verify, verify_peer} and cacertfile/cacerts is missing'

Cluster status of node rabbit@burn-e ...

00:25:02.569 [warn]  Description: 'Authenticity is not established by certificate path validation'
     Reason: 'Option {verify, verify_peer} and cacertfile/cacerts is missing'


00:25:02.716 [warn]  Description: 'Authenticity is not established by certificate path validation'
     Reason: 'Option {verify, verify_peer} and cacertfile/cacerts is missing'


00:25:02.895 [warn]  Description: 'Authenticity is not established by certificate path validation'
     Reason: 'Option {verify, verify_peer} and cacertfile/cacerts is missing'

Basics

Cluster name: rabbit@burn-e

Disk Nodes

rabbit@burn-e
rabbit@legion
rabbit@m-o
rabbit@matrix

Running Nodes

rabbit@burn-e
rabbit@legion
rabbit@m-o
rabbit@matrix

Versions

rabbit@burn-e: RabbitMQ 3.9.0 on Erlang 24.0.4
rabbit@legion: RabbitMQ 3.9.0 on Erlang 24.0.4
rabbit@m-o: RabbitMQ 3.9.0 on Erlang 24.0.4
rabbit@matrix: RabbitMQ 3.9.0 on Erlang 24.0.4

Maintenance status

Node: rabbit@burn-e, status: not under maintenance
Node: rabbit@legion, status: not under maintenance
Node: rabbit@m-o, status: not under maintenance
Node: rabbit@matrix, status: not under maintenance

Alarms

(none)

Network Partitions

(none)

Listeners

Node: rabbit@burn-e, interface: [::], port: 15672, protocol: http, purpose: HTTP API
Node: rabbit@burn-e, interface: [::], port: 15671, protocol: https, purpose: HTTP API over TLS (HTTPS)
Node: rabbit@burn-e, interface: [::], port: 25672, protocol: clustering, purpose: inter-node and CLI tool communication
Node: rabbit@burn-e, interface: [::], port: 5671, protocol: amqp/ssl, purpose: AMQP 0-9-1 and AMQP 1.0 over TLS
Node: rabbit@legion, interface: [::], port: 15672, protocol: http, purpose: HTTP API
Node: rabbit@legion, interface: [::], port: 15671, protocol: https, purpose: HTTP API over TLS (HTTPS)
Node: rabbit@legion, interface: [::], port: 25672, protocol: clustering, purpose: inter-node and CLI tool communication
Node: rabbit@legion, interface: [::], port: 5671, protocol: amqp/ssl, purpose: AMQP 0-9-1 and AMQP 1.0 over TLS
Node: rabbit@m-o, interface: [::], port: 15672, protocol: http, purpose: HTTP API
Node: rabbit@m-o, interface: [::], port: 15671, protocol: https, purpose: HTTP API over TLS (HTTPS)
Node: rabbit@m-o, interface: [::], port: 25672, protocol: clustering, purpose: inter-node and CLI tool communication
Node: rabbit@m-o, interface: [::], port: 5671, protocol: amqp/ssl, purpose: AMQP 0-9-1 and AMQP 1.0 over TLS
Node: rabbit@matrix, interface: [::], port: 15672, protocol: http, purpose: HTTP API
Node: rabbit@matrix, interface: [::], port: 15671, protocol: https, purpose: HTTP API over TLS (HTTPS)
Node: rabbit@matrix, interface: [::], port: 25672, protocol: clustering, purpose: inter-node and CLI tool communication
Node: rabbit@matrix, interface: [::], port: 5671, protocol: amqp/ssl, purpose: AMQP 0-9-1 and AMQP 1.0 over TLS

Feature flags

Flag: drop_unroutable_metric, state: disabled
Flag: empty_basic_get_metric, state: disabled
Flag: implicit_default_bindings, state: enabled
Flag: maintenance_mode_status, state: enabled
Flag: quorum_queue, state: enabled
Flag: stream_queue, state: disabled
Flag: user_limits, state: enabled
Flag: virtual_host_metadata, state: enabled
''', 'utf-8')
RABBITMQCTL_CLUSTER_STATUS_EVERYTHING_WRONG = bytes('''
00:25:02.415 [warn]  Description: 'Authenticity is not established by certificate path validation'
     Reason: 'Option {verify, verify_peer} and cacertfile/cacerts is missing'

Cluster status of node rabbit@burn-e ...

00:25:02.569 [warn]  Description: 'Authenticity is not established by certificate path validation'
     Reason: 'Option {verify, verify_peer} and cacertfile/cacerts is missing'


00:25:02.716 [warn]  Description: 'Authenticity is not established by certificate path validation'
     Reason: 'Option {verify, verify_peer} and cacertfile/cacerts is missing'


00:25:02.895 [warn]  Description: 'Authenticity is not established by certificate path validation'
     Reason: 'Option {verify, verify_peer} and cacertfile/cacerts is missing'

Basics

Cluster name: rabbit@burn-e

Disk Nodes

rabbit@burn-e
rabbit@m-o
rabbit@matrix

Running Nodes

rabbit@burn-e
rabbit@m-o
rabbit@matrix

Versions

rabbit@burn-e: RabbitMQ 3.9.1 on Erlang 24.0.4
rabbit@legion: RabbitMQ 3.9.0 on Erlang 24.0.4
rabbit@m-o: RabbitMQ 3.9.0 on Erlang 24.0.4
rabbit@matrix: RabbitMQ 3.9.0 on Erlang 24.0.4

Maintenance status

Node: rabbit@burn-e, status: not under maintenance
Node: rabbit@legion, status: not under maintenance
Node: rabbit@m-o, status: not under maintenance
Node: rabbit@matrix, status: not under maintenance

Alarms

Stuff

Network Partitions

Stuff

Listeners

Node: rabbit@burn-e, interface: [::], port: 15672, protocol: http, purpose: HTTP API
Node: rabbit@burn-e, interface: [::], port: 15671, protocol: https, purpose: HTTP API over TLS (HTTPS)
Node: rabbit@burn-e, interface: [::], port: 25672, protocol: clustering, purpose: inter-node and CLI tool communication
Node: rabbit@burn-e, interface: [::], port: 5671, protocol: amqp/ssl, purpose: AMQP 0-9-1 and AMQP 1.0 over TLS
Node: rabbit@legion, interface: [::], port: 15672, protocol: http, purpose: HTTP API
Node: rabbit@legion, interface: [::], port: 15671, protocol: https, purpose: HTTP API over TLS (HTTPS)
Node: rabbit@legion, interface: [::], port: 25672, protocol: clustering, purpose: inter-node and CLI tool communication
Node: rabbit@legion, interface: [::], port: 5671, protocol: amqp/ssl, purpose: AMQP 0-9-1 and AMQP 1.0 over TLS
Node: rabbit@m-o, interface: [::], port: 15672, protocol: http, purpose: HTTP API
Node: rabbit@m-o, interface: [::], port: 15671, protocol: https, purpose: HTTP API over TLS (HTTPS)
Node: rabbit@m-o, interface: [::], port: 25672, protocol: clustering, purpose: inter-node and CLI tool communication
Node: rabbit@m-o, interface: [::], port: 5671, protocol: amqp/ssl, purpose: AMQP 0-9-1 and AMQP 1.0 over TLS
Node: rabbit@matrix, interface: [::], port: 15672, protocol: http, purpose: HTTP API
Node: rabbit@matrix, interface: [::], port: 15671, protocol: https, purpose: HTTP API over TLS (HTTPS)
Node: rabbit@matrix, interface: [::], port: 25672, protocol: clustering, purpose: inter-node and CLI tool communication
Node: rabbit@matrix, interface: [::], port: 5671, protocol: amqp/ssl, purpose: AMQP 0-9-1 and AMQP 1.0 over TLS

Feature flags

Flag: drop_unroutable_metric, state: disabled
Flag: empty_basic_get_metric, state: disabled
Flag: implicit_default_bindings, state: enabled
Flag: maintenance_mode_status, state: enabled
Flag: quorum_queue, state: enabled
Flag: stream_queue, state: disabled
Flag: user_limits, state: enabled
Flag: virtual_host_metadata, state: enabled
''', 'utf-8')


def test_basic_status():
    rmq_status = RabbitMQClusterStatus(RABBITMQCTL_CLUSTER_STATUS.decode())
    assert rmq_status.cluster_name == 'rabbit@burn-e'


def test_disk_nodes_alert():
    rmq_status = RabbitMQClusterStatus(RABBITMQCTL_CLUSTER_STATUS.decode())
    alert = rmq_status.alert_for_disk_nodes(['rabbit@matrix', 'rabbit@skynet'], ttl=1,
                                            priority=Priority.WARNING)
    assert alert.description == 'Not all expected disk nodes are available in rabbit@burn-e cluster'


def test_running_nodes_alert():
    rmq_status = RabbitMQClusterStatus(RABBITMQCTL_CLUSTER_STATUS.decode())
    alert = rmq_status.alert_for_running_nodes(['rabbit@matrix', 'rabbit@skynet'], ttl=1,
                                               priority=Priority.WARNING)
    assert alert.description == 'Not all expected running nodes are available in rabbit@burn-e cluster'


def test_alarms_alert():
    rmq_status = RabbitMQClusterStatus(RABBITMQCTL_CLUSTER_STATUS_EVERYTHING_WRONG.decode())
    alert = rmq_status.alert_for_alarms(ttl=1, priority=Priority.WARNING)
    assert alert.description == 'RabbitMQ Alarms in rabbit@burn-e cluster'


def test_network_partition_alert():
    rmq_status = RabbitMQClusterStatus(RABBITMQCTL_CLUSTER_STATUS_EVERYTHING_WRONG.decode())
    alert = rmq_status.alert_for_network_partition(ttl=1, priority=Priority.WARNING)
    assert alert.description == 'RabbitMQ network partition in rabbit@burn-e cluster'


def test_version_mismatch_alert():
    rmq_status = RabbitMQClusterStatus(RABBITMQCTL_CLUSTER_STATUS_EVERYTHING_WRONG.decode())
    alert = rmq_status.alert_for_version_mismatch(ttl=1, priority=Priority.WARNING)
    assert alert.description == 'RabbitMQ versions in rabbit@burn-e do not match'


def test_no_disk_nodes_alert():
    rmq_status = RabbitMQClusterStatus(RABBITMQCTL_CLUSTER_STATUS.decode())
    assert rmq_status.alert_for_disk_nodes(['rabbit@matrix', 'rabbit@burn-e'], ttl=1,
                                           priority=Priority.WARNING) is None


def test_no_running_nodes_alert():
    rmq_status = RabbitMQClusterStatus(RABBITMQCTL_CLUSTER_STATUS.decode())
    assert rmq_status.alert_for_running_nodes(['rabbit@matrix', 'rabbit@burn-e'], ttl=1,
                                              priority=Priority.WARNING) is None


def test_no_alarms_alert():
    rmq_status = RabbitMQClusterStatus(RABBITMQCTL_CLUSTER_STATUS.decode())
    assert rmq_status.alert_for_alarms(ttl=1, priority=Priority.WARNING) is None


def test_low_priority_disk_nodes_alert():
    rmq_status = RabbitMQClusterStatus(RABBITMQCTL_CLUSTER_STATUS.decode())
    assert rmq_status.alert_for_disk_nodes(['rabbit@matrix', 'rabbit@burn-e'], ttl=1,
                                           priority=Priority.ACTIVITY) is None


def test_low_priority_running_nodes_alert():
    rmq_status = RabbitMQClusterStatus(RABBITMQCTL_CLUSTER_STATUS.decode())
    assert rmq_status.alert_for_running_nodes(['rabbit@matrix', 'rabbit@burn-e'], ttl=1,
                                              priority=Priority.ACTIVITY) is None


def test_low_priority_alarms_alert():
    rmq_status = RabbitMQClusterStatus(RABBITMQCTL_CLUSTER_STATUS.decode())
    assert rmq_status.alert_for_alarms(ttl=1, priority=Priority.ACTIVITY) is None


def test_low_priority_network_partition_alert():
    rmq_status = RabbitMQClusterStatus(RABBITMQCTL_CLUSTER_STATUS.decode())
    assert rmq_status.alert_for_network_partition(ttl=1, priority=Priority.ACTIVITY) is None


def test_low_priority_version_mismatch_alert():
    rmq_status = RabbitMQClusterStatus(RABBITMQCTL_CLUSTER_STATUS.decode())
    assert rmq_status.alert_for_version_mismatch(ttl=1, priority=Priority.ACTIVITY) is None


def test_rabbitmqctl_status(mocker: MockerFixture):
    mocker.patch('subprocess.check_output', return_value=RABBITMQCTL_CLUSTER_STATUS)
    rmq_status = cluster_status()
    assert rmq_status.alert_for_version_mismatch(ttl=1, priority=Priority.ACTIVITY) is None


def test_disk_node_expectations():
    exp = disk_node_expectations({"ttl": 60,
                                  "expected_disk_nodes": ["rabbit@burn-e", "rabbit@legion",
                                                          "rabbit@m-o", "rabbit@matrix"],
                                  "alert_priorities": {"missing_disk_node": "error"}})
    assert exp[0] == ["rabbit@burn-e", "rabbit@legion", "rabbit@m-o", "rabbit@matrix"]
    assert exp[1] == 60
    assert exp[2] == Priority.ERROR
    assert disk_node_expectations({}) is None


def test_running_node_expectations():
    exp = running_node_expectations({"ttl": 60,
                                     "expected_running_nodes": ["rabbit@burn-e", "rabbit@legion",
                                                                "rabbit@m-o", "rabbit@matrix"],
                                     "alert_priorities": {"missing_running_node": "error"}})
    assert exp[0] == ["rabbit@burn-e", "rabbit@legion", "rabbit@m-o", "rabbit@matrix"]
    assert exp[1] == 60
    assert exp[2] == Priority.ERROR
    assert running_node_expectations({}) is None


def test_version_mismatch_priority():
    assert Priority.WARNING == version_mismatch_priority({"ttl": 60,
                                                          "alert_priorities": {"version_mismatch": "warning"}})
    assert version_mismatch_priority({}) is None


def test_alarm_priority():
    assert Priority.CRITICAL == alarm_priority({"ttl": 60,
                                                "alert_priorities": {"alarm": "critical"}})
    assert alarm_priority({}) is None


def test_network_partition_priority():
    assert Priority.ERROR == network_partition_priority({"ttl": 60,
                                                         "alert_priorities": {"network_partition": "error"}})
    assert network_partition_priority({}) is None


def test_run_all_alerts(mocker):
    config = {"exchange": "skynet",
              "route": "mr-bones.service.rabbitmq",
              "ttl": 60,
              "expected_disk_nodes": ["rabbit@burn-e", "rabbit@legion",
                                      "rabbit@m-o", "rabbit@matrix"],
              "expected_running_nodes": ["rabbit@burn-e", "rabbit@legion",
                                         "rabbit@m-o", "rabbit@matrix"],
              "alert_priorities": {
                  "missing_disk_node": "error",
                  "missing_running_node": "error",
                  "version_mismatch": "warning",
                  "alarm": "critical",
                  "network_partition": "error"
              }}

    def mock_broadcast_alert_msg(exchange: str, route: str, alert):
        assert exchange == config['exchange']
        assert route == config['route']
        assert alert.priority >= Priority.WARNING
        assert alert.ttl == config['ttl']
    mocker.patch('subprocess.check_output', return_value=RABBITMQCTL_CLUSTER_STATUS_EVERYTHING_WRONG)
    mocker.patch('legion_utils.broadcast_alert_msg', mock_broadcast_alert_msg)
    run(config)


def test_run_no_alerts(mocker):
    config = {"exchange": "skynet",
              "route": "mr-bones.service.rabbitmq",
              "ttl": 60,
              "expected_disk_nodes": ["rabbit@burn-e", "rabbit@legion", "rabbit@m-o", "rabbit@matrix"],
              "expected_running_nodes": ["rabbit@burn-e", "rabbit@legion", "rabbit@m-o", "rabbit@matrix"],
              "alert_priorities": {
                  "missing_disk_node": "error",
                  "missing_running_node": "error",
                  "version_mismatch": "warning",
                  "alarm": "critical",
                  "network_partition": "error"
              }}

    def mock_broadcast_alert_msg(_, __, ___):
        assert False
    mocker.patch('subprocess.check_output', return_value=RABBITMQCTL_CLUSTER_STATUS)
    mocker.patch('legion_utils.broadcast_alert_msg', mock_broadcast_alert_msg)
    run(config)
