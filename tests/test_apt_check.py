from functools import partial
from socket import gethostname
from subprocess import PIPE

from pytest import mark
from pytest_mock import MockerFixture

import legion_utils

from reporters.apt_check import run

UPGRADE_LINE = 'qbittorrent-nox/focal 1:4.3.6.99~202107121017-7389-3ac8c97e6~ubuntu20.04.1 amd64 [upgradable from: 1:4.3.6.99~202107050919-7388-ede42910d~ubuntu20.04.1]'
SECURITY_LINE = 'python3-distupgrade/focal-security 1:20.04.35 all [upgradable from: 1:20.04.33]'


def mock_check_output_upgradable(num_u, num_s, cmd, stderr):
    assert cmd == ['apt', 'list', '--upgradable']
    assert stderr == PIPE
    return bytes('\n'.join(['Listing... Done'] + [UPGRADE_LINE] * (num_u - num_s) + [SECURITY_LINE] * num_s), 'utf-8')


CONFIG1 = {"security_warning_threshold": 3,
           "security_error_threshold": 10,
           "ttl": 600,
           "queue": "skynet",
           "route": "mr-bones.system.apt"}


@mark.parametrize('config,upgradable,security_upgradable', [
    (CONFIG1, 0, 0),
    (CONFIG1, 10, 2),
    (CONFIG1, 10, 3),
    (CONFIG1, 10, 10),
    (CONFIG1, 11, 11),
])
def test_apt_check(mocker: MockerFixture, config, upgradable, security_upgradable):
    def mock_broadcast_info(exchange, route, contents):
        assert exchange == config['queue']
        assert route == config['route']
        assert contents['upgradable'] == upgradable
        assert contents['security_upgradable'] == security_upgradable
        return None
    def mock_broadcast_warning(exchange, route, alert_key, desc, ttl, contents):
        assert alert_key == f'[{gethostname().lower()}][apt][security_upgradable]'
        assert desc == f'Too many apt security updates outstanding on {gethostname().lower()}'
        assert ttl == config['ttl']
        assert exchange == config['queue']
        assert route == config['route']
        assert contents['upgradable'] == upgradable
        assert contents['security_upgradable'] == security_upgradable
        assert contents['security_upgradable'] >= config['security_warning_threshold']
    def mock_broadcast_error(exchange, route, alert_key, desc, ttl, contents):
        assert alert_key == f'[{gethostname().lower()}][apt][security_upgradable]'
        assert desc == f'Too many apt security updates outstanding on {gethostname().lower()}'
        assert ttl == config['ttl']
        assert exchange == config['queue']
        assert route == config['route']
        assert contents['upgradable'] == upgradable
        assert contents['security_upgradable'] == security_upgradable
        assert contents['security_upgradable'] >= config['security_error_threshold']
    mocker.patch('legion_utils.broadcast_info', mock_broadcast_info)
    mocker.patch('legion_utils.broadcast_warning', mock_broadcast_warning)
    mocker.patch('legion_utils.broadcast_error', mock_broadcast_error)
    mocker.patch('subprocess.check_output', partial(mock_check_output_upgradable, upgradable, security_upgradable))
    run(config)