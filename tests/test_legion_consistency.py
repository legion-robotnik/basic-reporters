from socket import gethostname

from legion_utils import Priority
from pytest_mock import MockerFixture
from robotnikmq import Message

from reporters.legion_consistency import run

CONFIG1 = {"observe_exchange": "skynet",
           "observe_binding": "#",
           "report_exchange": "skynet.legion",
           "report_route": "mr-bones.meta.legion_consistency",
           "priority": "warning",
           "ttl": 60}


def test_run(mocker: MockerFixture):
    def mock_bind(self, exchange, binding):
        assert exchange == CONFIG1['observe_exchange']
        assert binding == CONFIG1['observe_binding']
        return self

    def mock_consume(_):
        for i in [None, Message({'stuff': 3}, routing_key='test'),
                  Message({'priority': 1}, routing_key='test'),
                  Message({'priority': 3}, routing_key='test'),
                  Message({'priority': 3, 'description': 'test', 'alert_key': '[something]', 'ttl': 5}, routing_key='test')]:
            yield i

    def mock_broadcast_msg(exchange, route, notification):
        assert exchange == CONFIG1['report_exchange']
        assert route == CONFIG1['report_route']
        assert notification.contents['src'] == gethostname()
        assert notification.desc in [f'Message of priority 3 is missing {i}' for i in ['alert key', 'description', 'TTL']]
        assert notification.alert_key in [f'[test][alert_without_{i}]' for i in ['key', 'description', 'ttl']]
        assert notification.priority == Priority.WARNING
        assert notification.ttl == CONFIG1['ttl']

    def mock_init(_):
        pass
    mocker.patch('legion_utils.broadcast_msg', mock_broadcast_msg)
    mocker.patch('reporters.legion_consistency.Subscriber.__init__', mock_init)
    mocker.patch('reporters.legion_consistency.Subscriber.bind', mock_bind)
    mocker.patch('reporters.legion_consistency.Subscriber.consume', mock_consume)
    run(CONFIG1)
