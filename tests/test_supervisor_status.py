from collections import namedtuple
from pprint import pprint
from socket import gethostname

import legion_utils
from pytest import mark
from pytest_mock import MockerFixture

from reporters.supervisor_status import run

SUPERVISOR_OUTPUT_1 = bytes('''
borg-backup                      RUNNING   pid 11571, uptime 3 days, 2:00:00
borg-backup-to-ed-209            RUNNING   pid 11627, uptime 3 days, 1:59:58
caddy2                           RUNNING   pid 7229, uptime 3 days, 2:07:27
calibre-web                      RUNNING   pid 10330, uptime 3 days, 2:00:10
ebooks-rsync-cache               RUNNING   pid 10831, uptime 3 days, 2:00:07
etherpad                         RUNNING   pid 18599, uptime 2 days, 23:54:54
gallery-dl-bot                   RUNNING   pid 11837, uptime 3 days, 1:59:54
legiond                          RUNNING   pid 4676, uptime 23:32:53
ripme-bot                        RUNNING   pid 11935, uptime 3 days, 1:59:52
seafile-cli                      RUNNING   pid 10860, uptime 3 days, 2:00:05
start-seafile                    EXITED    Jul 10 10:41 PM
start-seahub                     EXITED    Jul 10 10:41 PM
stop-seafile                     STOPPED   Not started
stop-seahub                      STOPPED   Not started
syncthing                        RUNNING   pid 11038, uptime 3 days, 2:00:03
wiki.js                          RUNNING   pid 7230, uptime 3 days, 2:07:27
youtube-dl-bot                   RUNNING   pid 11801, uptime 3 days, 1:59:56

''', 'utf-8')
SUPERVISOR_OUTPUT_MALFORMED = bytes('''
borg-backup                      RUNNING   pid 11571, uptime 3 days, 2:00:00
borg-backup-to-ed-209            RUNNING   pid 11627, uptime 3 days, 1:59:58
caddy2                           RUNNING   pid 7229, uptime 3 days, 2:07:27
calibre-web                      NO        pid 10330, uptime 3 days, 2:00:10
ebooks-rsync-cache               RUNNING   pid 10831, uptime 3 days, 2:00:07
etherpad                         RUNNING   pid 18599, uptime 2 days, 23:54:54
gallery-dl-bot                   RUNNING   pid 11837, uptime 3 days, 1:59:54
legiond                          RUNNING   pid 4676, uptime 23:32:53
ripme-bot                        RUNNING   pid 11935, uptime 3 days, 1:59:52
seafile-cli                      RUNNING   pid 10860, uptime 3 days, 2:00:05
start-seafile                    EXITED    Jul 10 10:41 PM
start-seahub                     EXITED    Jul 10 10:41 PM
stop-seafile                     STOPPED   Not started
stop-seahub                      STOPPED   Not started
syncthing                        RUNNING   pid 11038, uptime 3 days, 2:00:03
wiki.js                          RUNNING   pid 7230, uptime 3 days, 2:07:27
youtube-dl-bot                   RUNNING   pid 11801, uptime 3 days, 1:59:56

''', 'utf-8')
SUPERVISOR_STOPPING_LEGIOND = bytes('''
borg-backup                      RUNNING   pid 11571, uptime 3 days, 2:00:00
borg-backup-to-ed-209            RUNNING   pid 11627, uptime 3 days, 1:59:58
caddy2                           RUNNING   pid 7229, uptime 3 days, 2:07:27
calibre-web                      RUNNING   pid 10330, uptime 3 days, 2:00:10
ebooks-rsync-cache               RUNNING   pid 10831, uptime 3 days, 2:00:07
etherpad                         RUNNING   pid 18599, uptime 2 days, 23:54:54
gallery-dl-bot                   RUNNING   pid 11837, uptime 3 days, 1:59:54
legiond                          STOPPING  pid 4676, uptime 23:32:53
ripme-bot                        RUNNING   pid 11935, uptime 3 days, 1:59:52
seafile-cli                      RUNNING   pid 10860, uptime 3 days, 2:00:05
start-seafile                    EXITED    Jul 10 10:41 PM
start-seahub                     EXITED    Jul 10 10:41 PM
stop-seafile                     STOPPED   Not started
stop-seahub                      STOPPED   Not started
syncthing                        RUNNING   pid 11038, uptime 3 days, 2:00:03
wiki.js                          RUNNING   pid 7230, uptime 3 days, 2:07:27
youtube-dl-bot                   RUNNING   pid 11801, uptime 3 days, 1:59:56

''', 'utf-8')
CONFIG_1 = {"exchange": "skynet",
            "route": "my_server.system.supervisor",
            "ttl": 60,
            "processes": {
                "caddy2": {
                    "STOPPED": "warning",
                    "BACKOFF": "error",
                    "STOPPING": "warning",
                    "EXITED": "error",
                    "FATAL": "error",
                    "UNKNOWN": "error"
                },
                "legiond": {
                    "STOPPED": "error",
                    "BACKOFF": "critical",
                    "STOPPING": "activity",
                    "EXITED": "critical",
                    "FATAL": "critical",
                    "UNKNOWN": "critical"
                }
            }}
CONFIG_NON_EXISTENT_SERVICE = {"exchange": "skynet",
                               "route": "my_server.system.supervisor",
                               "ttl": 60,
                               "processes": {
                                   "cadd2": {
                                       "STOPPED": "warning",
                                       "BACKOFF": "error",
                                       "STOPPING": "warning",
                                       "EXITED": "error",
                                       "FATAL": "error",
                                       "UNKNOWN": "error"
                                   }
                               }}
CONFIG_NO_LIKE_SEAFILE_EXITED = {"exchange": "skynet",
                                 "route": "my_server.system.supervisor",
                                 "ttl": 60,
                                 "processes": {
                                     "start-seafile": {
                                         "STOPPED": "warning",
                                         "BACKOFF": "error",
                                         "STOPPING": "warning",
                                         "EXITED": "error",
                                         "FATAL": "error",
                                         "UNKNOWN": "error"
                                     }
                                 }}
CONFIG_CALIBRE_WEB = {"exchange": "skynet",
                      "route": "my_server.system.supervisor",
                      "ttl": 60,
                      "processes": {
                          "calibre-web": {
                              "STOPPED": "warning",
                              "BACKOFF": "error",
                              "STOPPING": "warning",
                              "EXITED": "error",
                              "FATAL": "error",
                              "UNKNOWN": "error"
                          }
                      }}

ProcResult = namedtuple('ProcResult', ['stdout'])


@mark.parametrize('sup_status,config,exp_info_count,exp_err_count,exp_alrt_count,exp_alrt_priority,exp_broadcast_count', [
    (SUPERVISOR_OUTPUT_1, CONFIG_1, 1, 0, 0, 0, 0),
    (SUPERVISOR_OUTPUT_1, CONFIG_NON_EXISTENT_SERVICE, 1, 1, 0, 0, 0),
    (SUPERVISOR_OUTPUT_1, CONFIG_NO_LIKE_SEAFILE_EXITED, 1, 0, 1, 3, 0),
    (SUPERVISOR_OUTPUT_MALFORMED, CONFIG_CALIBRE_WEB, 1, 1, 0, 0, 0),
    (SUPERVISOR_STOPPING_LEGIOND, CONFIG_1, 1, 0, 0, 1, 1),
])
def test_supervisor_status(mocker: MockerFixture, sup_status, config, exp_info_count,
                           exp_err_count, exp_alrt_count, exp_alrt_priority, exp_broadcast_count):
    mocker.patch('subprocess.run', return_value=ProcResult(sup_status))
    mocker.patch('legion_utils.broadcast_info', return_value=None)
    count = {'mock_broadcast_error': 0,
             'mock_broadcast_alert': 0,
             'mock_broadcast': 0}
    def mock_broadcast_error(exchange, route, desc, alert_key, contents, ttl):
        pprint(contents)
        assert exchange == config['exchange']
        assert route == config['route']
        assert ttl == config['ttl']
        assert desc.endswith(f'is not configured with supervisor on {gethostname()}')
        assert alert_key.startswith(f'[{gethostname()}][supervisor][') and alert_key.endswith(f'_not_configured]')
        count['mock_broadcast_error'] += 1
        return None
    mocker.patch('legion_utils.broadcast_error', mock_broadcast_error)
    def mock_broadcast_alert(exchange, route, priority, description, alert_key, contents, ttl):
        assert exchange == config['exchange']
        assert route == config['route']
        assert ttl == config['ttl']
        assert priority == exp_alrt_priority
        assert description.startswith(f'Supervisor-managed process: ')
        assert alert_key.startswith(f'[{gethostname()}][supervisor][')
        count['mock_broadcast_alert'] += 1
        return None
    mocker.patch('legion_utils.broadcast_alert', mock_broadcast_alert)
    def mock_broadcast(exchange, route, priority, contents, ttl):
        assert exchange == config['exchange']
        assert route == config['route']
        assert ttl == config['ttl']
        assert priority == exp_alrt_priority
        count['mock_broadcast'] += 1
        return None
    mocker.patch('legion_utils.broadcast', mock_broadcast)
    run(config)
    assert legion_utils.broadcast_info.call_count == exp_info_count
    assert count['mock_broadcast_error'] == exp_err_count
    assert count['mock_broadcast_alert'] == exp_alrt_count
    assert count['mock_broadcast'] == exp_broadcast_count
