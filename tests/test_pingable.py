from collections import namedtuple
from socket import gethostname
from subprocess import PIPE

import legion_utils
# import psutil
from pytest import mark
from pytest_mock import MockerFixture

from reporters.pingable import run

ProcResult = namedtuple('ProcResult', ['returncode'])


@mark.parametrize("config,mock_pingable",
                  [({"ping": ['1.1.1.1'], "queue": "skynet",
                     "count": 1, "route": "megabot.network.pingable"},
                    {'1.1.1.1': True}),
                   ({"ping": ['1.1.1.1', '2.2.2.2'], "queue": "skynet",
                     "count": 1, "route": "megabot.network.pingable"},
                    {'1.1.1.1': True, '2.2.2.2': False}),
                   ])
def test_system_base_stats(mocker: MockerFixture, config, mock_pingable):
    mocker.patch('legion_utils.broadcast_info', return_value=None)
    def mock_subprocess_run(cmd, stdout, stderr, timeout):
        assert stdout == PIPE
        assert stderr == PIPE
        assert timeout == 15
        assert cmd[:-1] == ['ping', '-c', str(config['count']), '-w', '10']
        return ProcResult(int(not mock_pingable[cmd[-1]]))
    mocker.patch('subprocess.run', mock_subprocess_run)
    run(config)
    contents = {'src': gethostname(),
                'pingable': {target: mock_pingable[target] for target in config['ping']}}
    legion_utils.broadcast_info.assert_called_once_with(config['queue'],
                                                        config['route'],
                                                        contents=contents)
