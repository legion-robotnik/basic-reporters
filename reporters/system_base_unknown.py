"""
This is a feedback reporter, designed to gather up system information from other reporters
analyze it, and fire alerts when a given system has been down for too long (for a configured
value of "too long"). This may imply that a given machine is down, however, this reporter does
not guarantee that, it only states that it has not received any system status updates for a given
amount of time.

For Example:

[reporter:system-reporter-feedback]
file_url=https://gitlab.com/legion-robotnik/basic-reporters/-/raw/main/reporters/system_base_unknown.py
...
requirements=['arrow']
config={"observe_exchange": "skynet",
        "report_route_suffix": ".system.system_reporting",
        "ttl": 60,
        "hosts": {
            "example.com": {
                "exchange": "skynet",
                "binding": "example.com.system.system_base_stats.info",
                "warning_after_seconds": 60,
                "error_after_seconds": 300,
                "critical_after_seconds": 600
            },
            "another_host.example.com": {
                "exchange": "skynet",
                "binding": "another_host.example.com.system.system_base_stats.info",
                "warning_after_seconds": 60,
                "error_after_seconds": 300,
                "critical_after_seconds": 600
            }
        }}
...

In the configuration above, the reporter will observe two hosts: example.com and
another_host.example.com (see the system_base_stats.py reporter for how the hostname is determined).
When example.com has not reported its system_base_stats for 60 seconds, this will produce a warning
every 30 seconds (half the configured TTL), if it hasn't reported for 300 seconds, the reporter
will begin to produce an error, and a critical once it's been 600 seconds since a report was
received.
"""
from collections import namedtuple
from socket import gethostname
from typing import Dict, Union, Optional

import arrow
import legion_utils
from legion_utils import Priority, AlertMsg
from robotnikmq import Subscriber


def now() -> int:
    return arrow.now().int_timestamp


HOSTNAME = gethostname()
START = now()
Host = namedtuple('Host', ['last_observed', 'last_reported', 'config'])


def seconds_since_reported(host: Host) -> int:
    return (now() - host.last_reported)


def seconds_since_observed(host: Host) -> int:
    return (now() - host.last_observed)


class NetworkState:
    def __init__(self, hosts: Dict[str, Dict[str, Union[int, str]]]):
        self._hosts = {host.lower(): Host(0, 0, config) for host, config in hosts.items()}

    @property
    def hosts(self):
        return self._hosts.items()

    def __getitem__(self, hostname: str) -> Optional[Host]:
        return self._hosts[hostname] if hostname in self._hosts else None

    def __contains__(self, hostname: str) -> bool:
        return hostname in self._hosts

    def observed(self, hostname: str, when: int) -> None:
        if hostname in self:
            self._hosts[hostname] = Host(when, self._hosts[hostname].last_reported,
                                         self._hosts[hostname].config)

    def observe(self, hostname: str) -> None:
        return self.observed(hostname, now())

    def report(self, hostname: str) -> None:
        if hostname in self:
            self._hosts[hostname] = Host(self._hosts[hostname].last_observed, now(),
                                         self._hosts[hostname].config)

    def alert_for(self, hostname: str, ttl: int) -> Optional[AlertMsg]:
        if hostname in self:
            host = self._hosts[hostname]
            if 'critical_after_seconds' in host.config and seconds_since_observed(host) >= host.config['critical_after_seconds']:
                priority = Priority.CRITICAL
            elif 'error_after_seconds' in host.config and seconds_since_observed(host) >= host.config['error_after_seconds']:
                priority = Priority.ERROR
            elif 'warning_after_seconds' in host.config and seconds_since_observed(host) >= host.config['warning_after_seconds']:
                priority = Priority.WARNING
            else:
                return None
            desc = f'{hostname} has never been observed' if host.last_observed == 0 \
                   else f'{hostname} last reported its status {arrow.now().shift(seconds=(0 - seconds_since_observed(host))).humanize()}'
            return AlertMsg(contents={'src': HOSTNAME,
                                      'last_observed': host.last_observed},
                            key=f'[{hostname}][system_reporting_failure]',
                            desc=desc, ttl=ttl, priority=priority)
        return None

    def alert_on(self, hostname: str, exchange: str, route: str, ttl: int) -> Optional[AlertMsg]:
        if alert := self.alert_for(hostname, ttl):
            legion_utils.broadcast_alert_msg(exchange, route, alert)
            self.report(hostname)
            return alert
        return None


def run(config: Dict) -> None:
    network_state = NetworkState(config['hosts'])
    sub = Subscriber()

    for exchange, binding in {(h['exchange'], h['binding']) for h in config['hosts'].values()}:
        sub.bind(exchange, binding)
    for msg in sub.consume(inactivity_timeout=3):
        if msg is not None and 'hostname' in msg:
            network_state.observe(msg['hostname'].lower())
        for hostname, host in network_state.hosts:
            if (now() - host.last_reported) > (config['ttl'] // 2) and (now() - host.last_observed) > (config['ttl'] // 2) and (now() - START) >= (config['ttl'] * 2):
                network_state.alert_on(hostname, config['report_exchange'],
                                       hostname + config['report_route_suffix'], config['ttl'])
