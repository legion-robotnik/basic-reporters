"""
This reporter is designed to check that DNS resolution results are happening as expected. It can be configured with
DNS names and expected IP addresses for those DNS names, as well as expectations like that certain domains should fail.

For example:

[reporter:dns-resolver]
file_url=https://gitlab.com/legion-robotnik/basic-reporters/-/raw/main/reporters/dns_resolver.py
...
requirements=["dnspython"]
config={"exchange": "skynet",
        "route": "my_server.network.dns",
        "ttl": 60,
        "domains": {
          "ads.google.com": {"nameserver": "192.168.86.20", "should_resolve_to": "0.0.0.0", "on_fail": "error"},
          "google.com": {},
          "burn-e.skynet": {"should_resolve_to": "192.168.86.41", "on_fail": "error"},
          "pihole.megabot.skynet": {"should_resolve_to": "192.168.86.20", "on_fail": "warning"},
          "this-site-isnt-real.co.uk": {"nameserver": "8.8.8.8", "nxdomain": True, "on_fail": "critical"}
        }
       }
...

In the example above, we have a pihole set up as the nameserver at 192.168.86.20 and its supposed to return 0.0.0.0
for ads.google.com. At the same time, we just want google.com to work (it doesn't matter what IP it returns) and we
want burn-e.skynet and pihole.megabot.skynet to resolve to specific IP addresses on the local network even when a
nameserver is not specified. Finally, if for some reason 8.8.8.8 gives us a valid result for a non-existent site,
we should panic.

Notes:
- By default, a failure results in a broadcast warning

"""
from socket import gethostname
from typing import Dict, Union, Any

from dns.resolver import Resolver, NXDOMAIN
from legion_utils import broadcast_info, broadcast_alert, Priority, broadcast

def verify(domain: str, expectation: Dict[str, Union[str, bool]]) -> True:
    resolver = Resolver()
    nxdomain_result = False
    if 'nameserver' in expectation:
        resolver.nameservers = [expectation['nameserver']]
    try:
        ips = [r.to_text() for r in resolver.resolve(domain).rrset if r]
        if "should_resolve_to" in expectation:
            return expectation['should_resolve_to'] in ips
    except NXDOMAIN:
        nxdomain_result = True
    if 'nxdomain' in expectation:
        return expectation['nxdomain'] == nxdomain_result  # if we wanted nxdomain and we got it, then its verified
    else:
        return not nxdomain_result  # if we did not specify whether we want an nxdomain result, then assume we did not, and return true on any successful DNS query


def run(config: Dict[str, Any]):
    report = {'dns_expectations': {}, 'src': gethostname()}
    for domain, expectation in config['domains'].items():
        report['dns_expectations'][domain] = verify(domain, expectation)
    broadcast_info(exchange=config['exchange'], route=config['route'], contents=report)
    for failed_domain in [e for e in report['dns_expectations'] if not report['dns_expectations'][e]]:
        failed_expectation = config['domains'][failed_domain]
        priority = Priority.__members__[failed_expectation['on_fail'].upper()] if 'on_fail' in failed_expectation else Priority.WARNING
        if priority >= 2:
            broadcast_alert(exchange=config['exchange'],
                            route=config['route'],
                            priority=priority,
                            description=f'{gethostname()} failed a DNS resolution expectation for {failed_domain}',
                            alert_key=f'[{gethostname()}][failed_dns_expectation][{failed_domain}]',
                            contents={'failed_dns_expectation': failed_domain, 'src': gethostname()},
                            ttl=config['ttl'])
        elif priority > 0:
            broadcast(exchange=config['exchange'],
                      priority=priority,
                      route=config['route'],
                      contents=report,
                      ttl=config['ttl'])
