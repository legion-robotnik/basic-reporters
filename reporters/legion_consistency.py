"""
This is a feedback reporter, designed to monitor messages published to rabbitMQ exchanges
and alert on inconsistent properties. This feedback reporter is good for identifying
improperly written alerts, such as alerts with a priority of ERROR, WARNING, or CRITICAL
that lack other required properties such as TTL, description, or an alert_key. Note that
you can always configure multiple such reporters for different priorities of alerts on
different exchanges and bindings.

For Example:

[reporter:legion-consistency]
file_url=https://gitlab.com/legion-robotnik/basic-reporters/-/raw/main/reporters/legion_consistency.py
...
config={"observe_exchange": "skynet",
        "observe_binding": "#",
        "report_exchange": "skynet.legion",
        "report_route": "mr-bones.meta.legion_consistency",
        "priority": "warning",
        "ttl": 60}
...

In the configuration above, the reporter will monitor the skynet exchange (all routing
keys) and then, whenever an inconsistent message is received, a warning will be reported
to the skynet.legion exchange with the configured report_route.
"""
from socket import gethostname
from typing import Dict

import legion_utils
from legion_utils import Priority, NotificationMsg
from robotnikmq import Subscriber

HOSTNAME = gethostname()


def run(config: Dict) -> None:
    sub = Subscriber().bind(config['observe_exchange'], config['observe_binding'])
    exchange = config['report_exchange']
    route = config['report_route']
    priority = Priority.__members__[config['priority'].upper()]
    for msg in sub.consume():
        if msg is not None and 'priority' in msg:
            if msg['priority'] > 1:
                if 'alert_key' not in msg:
                    legion_utils.broadcast_msg(exchange,
                                               route,
                                               NotificationMsg(contents={'src': HOSTNAME,
                                                                         'problem_message': msg},
                                                               alert_key=f'[{msg.route}][alert_without_key]',
                                                               desc=f'Message of priority {msg["priority"]} is missing alert key',
                                                               ttl=config['ttl'],
                                                               priority=priority))
                if 'description' not in msg:
                    legion_utils.broadcast_msg(exchange,
                                               route,
                                               NotificationMsg(contents={'src': HOSTNAME,
                                                                         'problem_message': msg},
                                                               alert_key=f'[{msg.route}][alert_without_description]',
                                                               desc=f'Message of priority {msg["priority"]} is missing description',
                                                               ttl=config['ttl'],
                                                               priority=priority))
                if 'ttl' not in msg:
                    legion_utils.broadcast_msg(exchange,
                                               route,
                                               NotificationMsg(contents={'src': HOSTNAME,
                                                                         'problem_message': msg},
                                                               alert_key=f'[{msg.route}][alert_without_ttl]',
                                                               desc=f'Message of priority {msg["priority"]} is missing TTL',
                                                               ttl=config['ttl'],
                                                               priority=priority))
