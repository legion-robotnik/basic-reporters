'''
This reporter publishes information about the status of RabbitMQ running on the machine.
Note that rabbitmqctl needs to be installed and configured in order for this to work properly.

Example Configuration

[reporter:rabbitmq-status]
file_url=https://gitlab.com/legion-robotnik/basic-reporters/-/raw/main/reporters/rabbitmq_status.py
...
requirements=[]
config={"exchange": "skynet",
        "route": "mr-bones.service.rabbitmq",
        "ttl": 60,
        "expected_disk_nodes": ["rabbit@burn-e", "rabbit@legion", "rabbit@m-o", "rabbit@matrix"],
        "expected_running_nodes": ["rabbit@burn-e", "rabbit@legion", "rabbit@m-o", "rabbit@matrix"],
        "alert_priorities": {
            "missing_disk_node": "error",
            "missing_running_node": "error",
            "version_mismatch": "warning",
            "alarm": "critical",
            "network_partition": "error"
        }}
'''

from functools import partial
from re import compile as regex, S, M
import subprocess
from typing import Optional, List, Tuple

import legion_utils
from legion_utils import Priority, priority_of, AlertMsg

CLUSTER_STATUS_PAT = regex(r'Basics\n\n(?P<basics>.*)\n\nDisk Nodes\n\n(?P<disk_nodes>.*)\n\n'
                           'Running Nodes\n\n(?P<running_nodes>.*)\n\nVersions\n\n(?P<versions>.*)'
                           '\n\nMaintenance status\n\n(?P<maintenance_status>.*)\n\nAlarms\n\n'
                           '(?P<alarms>.*)\n\nNetwork Partitions\n\n(?P<netwok_partitions>.*)\n\n'
                           'Listeners\n\n(?P<listeners>.*)\n\nFeature flags\n\n'
                           '(?P<feature_flags>.*)', S | M)
MAINT_STATUS_PAT = regex(r'Node\: (?P<node>\S+),\s+status\: (?P<status>.+)')


class RabbitMQClusterStatus:
    def __init__(self, output: str):
        results = output.split('\n\n')[output.split('\n\n').index('Basics'):]
        self.basics = results[1]
        self.cluster_name = self.basics.split(':')[1].strip()
        self.disk_nodes = set(results[3].split('\n'))
        self.running_nodes = set(results[5].split('\n'))
        self.versions = {v.split(':')[0].strip(): v.split(':')[1].strip() for v in results[7].split('\n')}
        self.maint_status = {MAINT_STATUS_PAT.match(line).groups('node'):
                             MAINT_STATUS_PAT.match(line).groups('status')
                             for line in results[9].split('\n')}
        self.alarms = results[11].strip().split('\n')
        self.network_partitions = results[13].strip().split('\n')
        self.feature_flags = set(results[15].split('\n'))
        self.listeners = set(results[17].split('\n'))

    def alert_for_disk_nodes(self, expected_nodes: List[str], ttl: int,
                             priority: Optional[Priority] = None) -> Optional[AlertMsg]:
        priority = priority or Priority.WARNING
        if priority >= Priority.WARNING and not set(self.disk_nodes) >= set(expected_nodes):
            return AlertMsg(contents={'expected_disk_nodes': expected_nodes,
                                      'actual_disk_nodes': list(self.disk_nodes)},
                            key=f'[{self.cluster_name}][rabbitmq][disk_node_mismatch]',
                            desc=f'Not all expected disk nodes are available in {self.cluster_name} cluster',
                            ttl=ttl, priority=priority)
        return None

    def alert_for_running_nodes(self, expected_nodes: List[str], ttl: int,
                                priority: Optional[Priority] = None) -> Optional[AlertMsg]:
        priority = priority or Priority.WARNING
        if priority >= Priority.WARNING and not set(self.running_nodes) >= set(expected_nodes):
            return AlertMsg(contents={'expected_running_nodes': expected_nodes,
                                      'actual_running_nodes': list(self.running_nodes)},
                            key=f'[{self.cluster_name}][rabbitmq][running_node_mismatch]',
                            desc=f'Not all expected running nodes are available in {self.cluster_name} cluster',
                            ttl=ttl, priority=priority)
        return None

    def alert_for_alarms(self, ttl: int, priority: Optional[Priority] = None) -> Optional[AlertMsg]:
        priority = priority or Priority.WARNING
        if priority >= Priority.WARNING and self.alarms != ['(none)']:
            return AlertMsg(contents={'alarms': self.alarms},
                            key=f'[{self.cluster_name}][rabbitmq][alarms]',
                            desc=f'RabbitMQ Alarms in {self.cluster_name} cluster',
                            ttl=ttl, priority=priority)
        return None

    def alert_for_network_partition(self, ttl: int,
                                    priority: Optional[Priority] = None) -> Optional[AlertMsg]:
        priority = priority or Priority.WARNING
        if priority >= Priority.WARNING and self.network_partitions != ['(none)']:
            return AlertMsg(contents={'network_partitions': self.network_partitions},
                            key=f'[{self.cluster_name}][rabbitmq][network_partitions]',
                            desc=f'RabbitMQ network partition in {self.cluster_name} cluster',
                            ttl=ttl, priority=priority)
        return None

    def alert_for_version_mismatch(self, ttl: int,
                                   priority: Optional[Priority] = None) -> Optional[AlertMsg]:
        priority = priority or Priority.WARNING
        if len(set(self.versions.values())) > 1:
            return AlertMsg(contents={'versions': self.versions},
                            key=f'[{self.cluster_name}][rabbitmq][version_mismatch]',
                            desc=f'RabbitMQ versions in {self.cluster_name} do not match',
                            ttl=ttl, priority=priority)
        return None


def cluster_status() -> RabbitMQClusterStatus:
    return RabbitMQClusterStatus(subprocess.check_output(['rabbitmqctl', 'cluster_status'],
                                                         stderr=subprocess.STDOUT)
                                 .decode().replace('\x1b[1m', '').replace('\x1b[0m', ''))


def disk_node_expectations(config) -> Optional[Tuple[List[str], int, Priority]]:
    if 'expected_disk_nodes' in config and 'missing_disk_node' in config['alert_priorities']:
        return (config['expected_disk_nodes'], config['ttl'],
                priority_of(config['alert_priorities']['missing_disk_node']))
    return None


def running_node_expectations(config) -> Optional[Tuple[List[str], int, Priority]]:
    if 'expected_running_nodes' in config and 'missing_running_node' in config['alert_priorities']:
        return (config['expected_running_nodes'], config['ttl'],
                priority_of(config['alert_priorities']['missing_running_node']))
    return None


def version_mismatch_priority(config) -> Optional[Priority]:
    if 'alert_priorities' in config and 'version_mismatch' in config['alert_priorities']:
        return priority_of(config['alert_priorities']['version_mismatch'])
    return None


def alarm_priority(config) -> Optional[Priority]:
    if 'alert_priorities' in config and 'alarm' in config['alert_priorities']:
        return priority_of(config['alert_priorities']['alarm'])
    return None


def network_partition_priority(config) -> Optional[Priority]:
    if 'alert_priorities' in config and 'network_partition' in config['alert_priorities']:
        return priority_of(config['alert_priorities']['network_partition'])
    return None


def run(config) -> None:
    status = cluster_status()
    broadcast = partial(legion_utils.broadcast_alert_msg, config['exchange'], config['route'])
    if (exp := disk_node_expectations(config)) and (alert := status.alert_for_disk_nodes(*exp)):
        broadcast(alert)
    if (exp := running_node_expectations(config)) and \
       (alert := status.alert_for_running_nodes(*exp)):
        broadcast(alert)
    if (priority := version_mismatch_priority(config)) and \
       (alert := status.alert_for_version_mismatch(config['ttl'], priority)):
        broadcast(alert)
    if (priority := alarm_priority(config)) and \
       (alert := status.alert_for_alarms(config['ttl'], priority)):
        broadcast(alert)
    if (priority := network_partition_priority(config)) and \
       (alert := status.alert_for_network_partition(config['ttl'], priority)):
        broadcast(alert)
