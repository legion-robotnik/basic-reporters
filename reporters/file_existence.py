'''
This reporter is designed to alert on files/directories that either exist or not. Its
useful for determining whether certain file systems are mounted, or can be used to report
or alert on software which uses the file system as an interface.

For example:

file_url=https://gitlab.com/legion-robotnik/basic-reporters/-/raw/main/reporters/file_existence.py
...
config={"exchange": "skynet",
        "route": "mr-bones.system.files",
        "ttl": 60,
        "paths": {
            "/mnt/remote-drive": {"state": "present",
                                  "priority": "activity"},
            "/usr/bin": {"state": "absent",
                         "priority": "critical"},
            "/usr/bin/python3": {"state": "absent",
                                 "priority": "error"},
            "/this/path/should/not/exist": {"state": "present",
                                            "priority": "error"}}}

The example above would publish activity information if the directory /mnt/remote-drive
exists (which might imply that something is mounted), it would publish a critical alert
if "/usr/bin" does not exist, an error if "/usr/bin/python3" does not exist, and an
error if "/this/path/should/not/exist" is present.
'''
from pathlib import Path
from socket import gethostname
from typing import Optional

import legion_utils
from legion_utils import Priority, NotificationMsg, priority_of


HOSTNAME = gethostname()


def notification_for(path: str, notify_if_present: bool, priority: Priority, ttl: int) -> Optional[NotificationMsg]:
    path_exists = (len([i for i in Path('/').glob(path.strip('/'))]) > 0) if '*' in path else Path(path).exists()
    if (path_exists and notify_if_present) or (not path_exists and not notify_if_present):
        return NotificationMsg(contents={'path': path, 'state': 'present' if path_exists else 'absent'},
                               alert_key=[HOSTNAME, str(path), "present" if path_exists else "absent"] if priority >= Priority.WARNING else None,
                               desc=f'{HOSTNAME}:{path} {"present" if path_exists else "absent"}' if priority >= Priority.WARNING else None,
                               ttl=ttl, priority=priority)
    return None


def run(config) -> None:
    for path in config['paths']:
        if msg := notification_for(path,
                                   notify_if_present=(True if (config['paths'][path]['state'] == 'present')
                                                      else (False if config['paths'][path]['state'] == 'absent' else None)),
                                   priority=priority_of(config['paths'][path]['priority']),
                                   ttl=config['ttl']):
            legion_utils.broadcast_msg(config['exchange'], config['route'], msg)
