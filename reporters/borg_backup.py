'''
This reporter is designed to monitor the performance of borg backups (https://borgbackup.readthedocs.io/en/stable/index.html)
Specifically, whether they are happening with the correct frequency. Multiple repositories may be configured and will be
checked as needed, as well as appropriate thresholds for how long ago the most recent successful backup occurred before
an alert should be fired.

For example:

requirements=["arrow"]
config={
    "exchange": "skynet",
    "route": "mr-bones.system.borg_backups",
    "ttl": 1200,
    "repositories": {
        "ed-209:/local/borg/mr-bones": {
            "passphrase": "hackme",
            "local_user": "eugene",
            "warn_threshold_seconds": 172800,
            "err_threshold_seconds": 432000,
            "crit_threshold_seconds": 604800
        },
        "ed-209:/local/borg/mr-bones": {
            "passphrase": "hackme",
            "local_user": "eugene",
            "warn_threshold_seconds": 172800,
            "err_threshold_seconds": 432000,
            "crit_threshold_seconds": 604800
        },
    },
}
'''
from collections import namedtuple
from functools import lru_cache
import os
from re import compile as regex
from socket import gethostname
import subprocess
from subprocess import PIPE
from typing import List, Optional, Dict, Any, Tuple

import arrow
import legion_utils
from legion_utils import Priority


Snapshot = namedtuple('Snapshot', ['name', 'timestamp', 'hash'])
BORG_LIST_PAT = regex(r'(?P<name>\S+)\s+\w\w\w, (?P<datetime>\d\d\d\d-\d\d-\d\d \d\d\:\d\d:\d\d)\s+\[(?P<hash>\w+)\].*')


def snapshots(borg_list_output: Optional[bytes]) -> List[Snapshot]:
    result = []
    if borg_list_output:
        for line in borg_list_output.decode().split('\n'):
            if match := BORG_LIST_PAT.match(line):
                result.append(Snapshot(match.group('name'),
                                       arrow.get(match.group('datetime'), tzinfo=arrow.now().timetz().tzinfo).int_timestamp,
                                       match.group('hash')))
    return result


@lru_cache
def cmd(as_user: Optional[str] = None) -> List[str]:
    return ['su', as_user, '-c', 'borg list'] if as_user is not None else ['bash', '-c', 'borg list']


@lru_cache
def env(repo: str, passphrase: str) -> Dict[str, Any]:
    return {**os.environ, 'BORG_REPO': repo, 'BORG_PASSPHRASE': passphrase}


def seconds_since_last(snaps: List[Snapshot]) -> int:
    return arrow.now().int_timestamp - max(snaps, key=lambda s: s.timestamp).timestamp


def humanize_latest(snaps: List[Snapshot]):
    return arrow.now().shift(seconds=-(seconds_since_last(snaps))).humanize()


def prio(seconds: int, repo: Dict) -> Priority:
    return (Priority.CRITICAL if seconds >= repo['crit_threshold_seconds'] else
            (Priority.ERROR if seconds >= repo['err_threshold_seconds'] else
             Priority.WARNING))


def delayed_snapshot(snaps: List[Snapshot], config, repo) -> Optional[Tuple[str, str, str, str, Dict[str, Any], int, Priority]]:
    repo_config = config['repositories'][repo]
    if seconds_since_last(snaps) > repo_config['warn_threshold_seconds']:
        return (config['exchange'], config['route'],
                f'Last successful backup from {gethostname()} to {repo} was {humanize_latest(snaps)}',
                f'[{gethostname()}][borg-backup][{repo}][too_long]', {'problem_repo': s._asdict() for s in snaps}, config['ttl'],
                prio(seconds_since_last(snaps), repo_config))
    return None


def borg_list(repo: str, passphrase: str, as_user: Optional[str] = None) -> List[Snapshot]:
    return snapshots(subprocess.run(cmd(as_user), env=env(repo, passphrase), check=False, stdout=PIPE).stdout)


def backups(repos):
    return {repo: borg_list(repo, repos[repo]['passphrase'], repos[repo]['local_user'] if 'local_user' in repos[repo] else None) for repo in repos}


def report(repos):
    return {repo: [s._asdict() for s in snaps] for repo, snaps in backups(repos).items()}


def run(config) -> None:
    repos = config['repositories']
    legion_utils.broadcast_info(config['exchange'], config['route'], {'repositories': report(repos),
                                                                      'src': gethostname()})
    for repo, snaps in [(r,s) for r,s in backups(repos).items() if r and s]:
        if alert_data := delayed_snapshot(snaps, config, repo):
            legion_utils.broadcast_alert(*alert_data)
