"""
This reporter takes a configuration object that is effectively a list of paths at which certificates
are to be found as well as the thresholds for expiry dates which are used to determine whether to
fire a warning, error, or critical. It also, obviously, takes a TTL, exchange, and route to use.

If a certificate expires in fewer days than a given threshold, then this reporter will fire an alert
of appropriate priority. Otherwise, it simply broadcasts info.

For example:

[reporter:cert_expiry]
file_url=https://gitlab.com/legion-robotnik/basic-reporters/-/raw/main/reporters/cert_expiry.py
...
requirements=["cryptography", "arrow"]
config={"exchange": "skynet",
        "route": "my_server.network.cert_expiry",
        "ttl": 600,
        "paths": ["/etc/tls/https.cert",
                  "/var/www/my_app/my_cert.pem"],
        "expiry_warning_threshold": 30,
        "expiry_error_threshold": 10,
        "expiry_critical_threshold": 1}
...

In this example, the reporter will check two different certificates, and if any of them expire in
less than or equal to 30 days, it will fire a warning, an error for 10 days, and a critical for one
day. Otherwise it will simply broadcast the information. It will also broadcast critical if a given
certificate file does not exist. Each alert will last 10 minutes (600 seconds) unless cleared.
"""

from datetime import datetime
from pathlib import Path
from socket import gethostname
from typing import Dict, List, Union

import arrow
from arrow.arrow import Arrow
from cryptography import x509
from legion_utils import broadcast_info, broadcast_warning, broadcast_error, broadcast_critical


def _not_valid_after(cert_path: Path) -> Arrow:
    cert = x509.load_pem_x509_certificate(cert_path.open('rb').read())
    return arrow.get(cert.not_valid_after)


def run(config: Dict[str, Union[List[str], int]]):
    for cert_file in config['paths']:
        cert_path = Path(cert_file).resolve()
        if not cert_path.exists():
            broadcast_error(exchange=config['exchange'],
                            route=config['route'],
                            desc=f'Certificate at {gethostname()}:{cert_path} does not exist',
                            alert_key=f'[{gethostname()}][{cert_path}][does_not_exist]',
                            contents={'src': gethostname(),
                                      'cert_path': str(cert_path)},
                            ttl=config['ttl'])
        else:
            not_valid_after = _not_valid_after(cert_path)
            expiry = (not_valid_after - arrow.now()).days
            report = {'src': gethostname(),
                      'cert_path': str(cert_path),
                      'not_valid_after': not_valid_after.int_timestamp}
            desc = f'Certificate at {gethostname()}:{cert_path} expires in {expiry} days' if \
                   expiry >= 0 else f'Certificate at {gethostname()}:{cert_path} has expired!'
            if config['expiry_critical_threshold'] >= expiry:
                broadcast_critical(exchange=config['exchange'],
                                   route=config['route'],
                                   desc=desc,
                                   alert_key=f'[{gethostname()}][{cert_path}][expiry]',
                                   contents=report,
                                   ttl=config['ttl'])
            elif config['expiry_error_threshold'] >= expiry:
                broadcast_error(exchange=config['exchange'],
                                route=config['route'],
                                desc=desc,
                                alert_key=f'[{gethostname()}][{cert_path}][expiry]',
                                contents=report,
                                ttl=config['ttl'])
            elif config['expiry_warning_threshold'] >= expiry:
                broadcast_warning(exchange=config['exchange'],
                                  route=config['route'],
                                  desc=desc,
                                  alert_key=f'[{gethostname()}][{cert_path}][expiry]',
                                  contents=report,
                                  ttl=config['ttl'])
            else:
                broadcast_info(exchange=config['exchange'], route=config['route'], contents=report)
