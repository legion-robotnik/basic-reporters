import socket
import subprocess
from typing import List

import legion_utils


def upgradable() -> List[str]:
    return [p for p in subprocess.check_output(['apt', 'list', '--upgradable'], stderr=subprocess.PIPE).decode().split('\n')[1:] if p]


def run(config):
    HOSTNAME = socket.gethostname().lower()
    report = {'host': HOSTNAME,
              'upgradable': len(upgradable()),
              'security_upgradable': len([p for p in upgradable() if '-security' in p])}
    if report['security_upgradable'] >= config['security_error_threshold']:
        legion_utils.broadcast_error(config['queue'], config['route'],
                                     alert_key=f'[{HOSTNAME}][apt][security_upgradable]',
                                     desc=f'Too many apt security updates outstanding on {HOSTNAME}',
                                     ttl=config['ttl'], contents=report)
    elif report['security_upgradable'] >= config['security_warning_threshold']:
        legion_utils.broadcast_warning(config['queue'], config['route'],
                                       alert_key=f'[{HOSTNAME}][apt][security_upgradable]',
                                       desc=f'Too many apt security updates outstanding on {HOSTNAME}',
                                       ttl=config['ttl'], contents=report)
    else:
        legion_utils.broadcast_info(config['queue'], config['route'], contents=report)
