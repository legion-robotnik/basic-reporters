"""
This reporter is designed to publish information on basic system stats, such as CPU/RAM consumption
and disk space usage. This reporter is also capable of alerting on low disk space when properly
configured with thresholds.

For example:

[reporter:system-base-stats]
file_url=https://gitlab.com/legion-robotnik/basic-reporters/-/raw/main/reporters/system_base_stats.py
...
requirements=["psutil", "arrow"]
config={"paths": ["/", "/local"],
        "queue": "skynet",
        "route": "my_server.system.system_base_stats"}
...

This example demonstrates a basic configuration which will report CPU/RAM/Disk information to the
"skynet" exchange under the routing key approriate for the server. Note that disk information is
determined by a list of paths at which the disks are mounted.

Alternatively:

[reporter:system-base-stats]
file_url=https://gitlab.com/legion-robotnik/basic-reporters/-/raw/main/reporters/system_base_stats.py
...
requirements=["psutil", "arrow"]
config={"paths": {"/": {'warning_threshold_used_percent': 90,
                        'error_threshold_used_percent': 95,
                        'critical_threshold_used_percent': 98}},
        "restart_required": {"severity": "warning", "path": "/var/run/reboot-required"}
        "queue": "skynet",
        "route": "mr-bones.system.system_base_stats"}
...

In this example, we use "paths" as a dictionary with thresholds, for warning, error, and critical.
Each threshold is optional and can be set to any percentage you want. If the disk utilization
becomes equal to or greater than the given threshold, an alert of appropriate priority will be
broadcast. You can also see the "restart_required" setting which will broadcast messages of the given
severity if the /var/run/reboot-required file is detected on the system.
"""
from pathlib import Path
import socket
from time import time

import arrow
import legion_utils
from legion_utils import priority_of, Priority
import psutil
from psutil import boot_time


def run(config):
    HOSTNAME = socket.gethostname().lower()
    report = {'hostname': HOSTNAME,
              'core_load': psutil.cpu_percent(interval=5, percpu=True),
              'CPU_load': psutil.cpu_percent(interval=5, percpu=False),
              'RAM_usage': psutil.virtual_memory().percent}
    if 'restart_required' in config:
        priority = priority_of(config['restart_required']['severity'])
        path = Path(config['restart_required']['path'])
        if path.exists():
            legion_utils.broadcast(exchange=config['queue'],
                                   priority=priority,
                                   route=config['route'],
                                   description=f'Restart required: {HOSTNAME}',
                                   alert_key=f'[{HOSTNAME}][restart_required]' if priority >= Priority.WARNING else None,
                                   contents={'hostname': HOSTNAME,
                                             'up_since': arrow.utcnow().shift(seconds=(boot_time() - time())).humanize(),
                                             'boot_time': boot_time()},
                                   ttl=60)
    if 'paths' in config:
        for path in config['paths']:
            if not Path(path).exists():
                legion_utils.broadcast_critical(exchange=config['queue'],
                                                route=config['route'],
                                                contents={'hostname': HOSTNAME,
                                                          'non_existent_path': path},
                                                desc=f'Disk mount at {path} on {HOSTNAME} does not exist',
                                                alert_key=f'[{HOSTNAME}][{path}][disk_does_not_exist]', ttl=60)
        report['disks'] = {path: dict(psutil.disk_usage(path)._asdict()) for path in config['paths'] if Path(path).exists()}
        for path, results in report['disks'].items():
            if not isinstance(config['paths'], list) and \
               isinstance(config['paths'], dict) and \
               isinstance(config['paths'][path], dict):
                path_config = config['paths'][path]
                if 'critical_threshold_used_percent' in path_config and path_config['critical_threshold_used_percent'] <= results['percent']:
                    legion_utils.broadcast_critical(exchange=config['queue'],
                                                    route=config['route'],
                                                    contents=results,
                                                    desc=f'Disk mounted at {path} on {HOSTNAME} is {results["percent"]}% full',
                                                    alert_key=f'[{HOSTNAME}][{path}][disk_usage_past_threshold]', ttl=60)
                elif 'error_threshold_used_percent' in path_config and path_config['error_threshold_used_percent'] <= results['percent']:
                    legion_utils.broadcast_error(exchange=config['queue'],
                                                 route=config['route'],
                                                 contents=results,
                                                 desc=f'Disk mounted at {path} on {HOSTNAME} is {results["percent"]}% full',
                                                 alert_key=f'[{HOSTNAME}][{path}][disk_usage_past_threshold]', ttl=60)
                elif 'warning_threshold_used_percent' in path_config and path_config['warning_threshold_used_percent'] <= results['percent']:
                    legion_utils.broadcast_warning(exchange=config['queue'],
                                                   route=config['route'],
                                                   contents=results,
                                                   desc=f'Disk mounted at {path} on {HOSTNAME} is {results["percent"]}% full',
                                                   alert_key=f'[{HOSTNAME}][{path}][disk_usage_past_threshold]', ttl=60)
    legion_utils.broadcast_info(exchange=config['queue'],
                                route=config['route'],
                                contents=report)
