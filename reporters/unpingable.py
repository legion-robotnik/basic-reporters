"""
This feedback reporter gathers statistics on which hosts are unpingable based on information from
the pingable reporter. It takes a configuration which tells it how many sources must be unable to
ping a given target before a warning, error, or critical alert is fired. Any number of sources that
cannot ping a given target will produce at least an info broadcast.

Example Configuration:

[reporter:unpingable-feedback]
file_url=https://gitlab.com/legion-robotnik/basic-reporters/-/raw/main/reporters/unpingable.py
description: Monitors reported pings and broadcasts alerts when too many machine cannot ping a given target
delay=30
requirements=["arrow"]
config={"observe_exchange": "skynet",
        "observe_binding": "#.network.pingable.info",
        "report_exchange": "skynet",
        "report_route_suffix": ".network.unpingable",
        "ttl": 60,
        "unpingable_from_critical": 4,
        "unpingable_from_error": 3,
        "unpingable_from_warning": 2,
       }

The example configuration above would monitor a given exchange/binding and when 4 machines cannot
ping the same target, this would set off a critical alert, 3 for an erro, and 2 for a warning. Hosts
that are targeted for pinging are gathered dynamically from the ping reports.
"""
from threading import Thread, Lock
from time import sleep
from typing import Dict

from arrow import now, Arrow
from legion_utils import broadcast_critical, broadcast_error, broadcast_warning, broadcast_info
from robotnikmq import Subscriber, Message


def run(config: Dict) -> None:
    obsrv_ex = config['observe_exchange']
    obsrv_bind = config['observe_binding']
    rprt_ex = config['report_exchange']
    rprt_route_suffix = config['report_route_suffix']

    ping_targets: Dict[str, Dict[str, bool]] = {}
    last_reported: Dict[str, Arrow] = {}
    network_state_lock = Lock()

    def sub(msg: Message) -> None:
        with network_state_lock:
            if 'pingable' in msg and 'src' in msg:
                src = msg['src']
                for target in msg['pingable']:
                    if target not in ping_targets:
                        ping_targets[target] = {}
                    ping_targets[target][src] = msg['pingable'][target]

    def run_sub():
        Subscriber().bind(obsrv_ex, obsrv_bind).run(sub)

    subscriber_thread = Thread(target=run_sub)
    subscriber_thread.start()

    while 42:
        sleep(1)
        with network_state_lock:
            for target, sources in ping_targets.items():
                if (target in last_reported and
                   (now() - last_reported[target]).seconds > (config['ttl'] // 2)) \
                  or target not in last_reported:
                    num_unpingable_sources = sum(int(not pingable) for _, pingable in sources.items())
                    if num_unpingable_sources >= config['unpingable_from_critical']:
                        broadcast_critical(exchange=rprt_ex,
                                           route=target + rprt_route_suffix,
                                           desc=f'{target} is unpingable from {num_unpingable_sources} hosts',
                                           alert_key=f'[{target}][unpingable]',
                                           contents={'target': target,
                                                     'sources': sources},
                                           ttl=config['ttl'])
                        last_reported[target] = now()
                    elif num_unpingable_sources >= config['unpingable_from_error']:
                        broadcast_error(exchange=rprt_ex,
                                        route=target + rprt_route_suffix,
                                        desc=f'{target} is unpingable from {num_unpingable_sources} hosts',
                                        alert_key=f'[{target}][unpingable]',
                                        contents={'target': target,
                                                  'sources': sources},
                                        ttl=config['ttl'])
                        last_reported[target] = now()
                    elif num_unpingable_sources >= config['unpingable_from_warning']:
                        broadcast_warning(exchange=rprt_ex,
                                          route=target + rprt_route_suffix,
                                          desc=f'{target} is unpingable from {num_unpingable_sources} hosts',
                                          alert_key=f'[{target}][unpingable]',
                                          contents={'target': target,
                                                    'sources': sources},
                                          ttl=config['ttl'])
                        last_reported[target] = now()
                    elif num_unpingable_sources > 0:
                        broadcast_info(exchange=rprt_ex,
                                       route=target + rprt_route_suffix,
                                       contents={'target': target,
                                                 'sources': sources})
                        last_reported[target] = now()
