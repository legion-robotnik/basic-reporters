import socket
import subprocess

import legion_utils


def pingable(host: str, count: int) -> bool:
    return subprocess.run(['ping', '-c', str(count), '-w', '10', host],
                          stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                          timeout=15).returncode == 0


def run(config):
    legion_utils.broadcast_info(config['queue'], config['route'],
                                contents={'src': socket.gethostname(),
                                          'pingable': {target: pingable(target, config['count']) for target in config['ping']}})
