"""
This reporter is designed to report on processes managed by supervisord: http://supervisord.org/

This reporter will broadcast information about all the processes running on the system via
supervisor, however, you can also configure specific process statuses to prompt alerts by
specifying a given status and the priority at which a message about it should be broadcast.
All unspecified statuses/processes will be broadcast at the lowest priority.

[reporter:supervisor-status]
file_url=https://gitlab.com/legion-robotnik/basic-reporters/-/raw/main/reporters/supervisor_status.py
...
config={"exchange": "skynet",
        "route": "my_server.system.supervisor",
        "ttl": 60,
        "processes": {
            "caddy2": {
                "STOPPED": "warning",
                "BACKOFF": "error",
                "STOPPING": "warning",
                "EXITED": "error",
                "FATAL": "error",
                "UNKNOWN": "error"
            },
            "legiond": {
                "STOPPED": "error",
                "BACKOFF": "critical",
                "STOPPING": "error",
                "EXITED": "critical",
                "FATAL": "critical",
                "UNKNOWN": "critical"
            }
        }}
...

In the example above, we care about 2 processes: caddy2 and legiond. Caddy is important, so whenever
its stopped/stopping, that's a warning, however, if its not running for any other reason, that's an
error. On the other hand, legiond is far more important (its how we monitor the system) so the
priorities for anything other than running/starting are error and critical.
"""
from re import compile as regex
from socket import gethostname
import subprocess
from typing import Dict, Union

import legion_utils
from legion_utils import Priority


HOSTNAME = gethostname()
STATUS_PAT = regex(r'(?P<process>\S+)\s+(?P<state>RUNNING|STARTING|STOPPED|BACKOFF|STOPPING|EXITED|FATAL|UNKNOWN)\s+(?P<status>(pid (?P<pid>\d+)\,\s+uptime (?P<uptime>.+))?.*)')


def sup_status() -> Dict[str, Union[str, Dict[str, Dict[str, str]]]]:
    report: Dict[str, Union[str, Dict[str, Dict[str, str]]]] = {'hostname': HOSTNAME}
    processes: Dict[str, Dict[str, str]] = {}
    report['processes'] = processes
    for line in subprocess.run(['supervisorctl', 'status'], capture_output=True).stdout.decode().split('\n'):
        if m := STATUS_PAT.match(line):
            processes[m.group('process')] = {'state': m.group('state'),
                                             'status': m.group('status')}
            if m.group('pid'):
                processes[m.group('process')]['pid'] = m.group('pid')
            if m.group('uptime'):
                processes[m.group('process')]['uptime'] = m.group('uptime')
    return report


def run(config):
    report = sup_status()
    legion_utils.broadcast_info(exchange=config['exchange'], route=config['route'], contents=report)
    for process_name in config['processes']:
        if process_name not in report['processes']:
            legion_utils.broadcast_error(exchange=config['exchange'],
                                         route=config['route'],
                                         desc=f'{process_name} is not configured with supervisor on {HOSTNAME}',
                                         alert_key=f'[{HOSTNAME}][supervisor][{process_name}_not_configured]',
                                         contents={'unconfigured_process': process_name,
                                                   'hostname': HOSTNAME},
                                         ttl=config['ttl'])
        else:
            state = report['processes'][process_name]['state']
            priority = Priority.__members__[config['processes'][process_name][state].upper()] if state in config['processes'][process_name] else Priority.INFO
            if priority >= 2:
                legion_utils.broadcast_alert(exchange=config['exchange'],
                                             route=config['route'],
                                             priority=priority,
                                             description=f'Supervisor-managed process: {process_name} is in an undesirable state: {state.upper()}',
                                             alert_key=f'[{HOSTNAME}][supervisor][{process_name}][{state}]',
                                             contents={'hostname': HOSTNAME,
                                                       'problematic_process': report['processes'][process_name]},
                                             ttl=config['ttl'])
            elif priority > 0:
                legion_utils.broadcast(exchange=config['exchange'],
                                       priority=priority,
                                       route=config['route'],
                                       contents={'hostname': HOSTNAME,
                                                 'problematic_process': report['processes'][process_name]},
                                       ttl=config['ttl'])
