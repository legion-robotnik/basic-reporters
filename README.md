# basic-reporters

A set of basic reporters useful for general monitoring of a given system or set of systems. Primarily useful as examples for how to write your own reporters and feedback monitors.

## Development

### Setup

You will likely find yourself quickly writing your own reporter scripts, and that's exactly the point, however, it is absolutely critical that reporter scripts be thoroughly tested. The following setup is geared towards helping you write and test your own reporter scripts quickly and easily.

1. Set up [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/) if you don't have it already.
    a. `virtualenvwrapper` gives you lots of useful stuff when installed correctly, such as the ability to get back to whatever virtual environment you want to work in by using the `workon <virtualenv>` command.

2. Create a virtualenv for your reporters. In our case that would be `mkvirtualenv -a . -p python3.8 basic-reporters` (from the repo directory). The documentation, from here on in, will preface any command that needs to be executed inside the virtualenv with `(basic-reporters)$`.

3. Install requirements: This one is a little bit more complicated. You need to install all the required packages in your virtualenv, and because this repository is not a standard python package, it does not list those requirements in any automated way. Luckily, you can just copy and paste this command in your virtualenv:

```
(basic-reporters)$ pip3 install --upgrade legion-utils robotnikmq psutil dnspython pytest pytest-cov pytest-mock
```

however, if you want to develop more reporters, you will need to add your own packages to that list.

#### IDE Setup

**Sublime Text 3**

```bash
(basic-reporters)$ curl -sSL https://gitlab.com/-/snippets/2153709/raw/main/virtualenvwrapper.sublime-project.py | python
```

### Testing

Once everything is set up, as described above, you should simply be able to execute `pytest` from the repo directory/virtualenv and it will run all the unit tests.

To run a specific unit test do:

```bash
(basic-reporters)$ pytest tests/test_system_base_stats.py::test_basic_run
```

Note that this will also print out coverage information (and generate an HTML coverage report in `htmlcov/index.html`). You should always strive to have 100% coverage on all of your reporters with unit tests.
